Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  devise_for :users, :controllers => {:confirmations => 'confirmations', :registrations => 'users' }

  devise_scope :user do
    patch "/confirm" => "confirmations#confirm"
  end
  resources :tunes
  resources :parts
  resources :users
  resources :groups
  resources :favorites
  resources :homeworks
  resources :messages
  resources :books do
   resources :chapters do
     resources :paragraphs
   end
  end
  resources :recordings
  resources :book_subscriptions
  resources :user_logs
  resources :annotations
  resources :questions
  resources :grooves

  root 'books#index'
  get 'practice' => "parts#practice", as: :tunes_practice
  get 'user_settings' => "home#user_settings", as: :user_settings
  get 'user_list' => "home#user_list", as: :user_list
  get 'interactive_excercises' => "home#interactive_excercises", as: :interactive_excercises
  get 'real_music' => "home#real_music", as: :real_music
  get 'pricing' => "home#pricing", as: :pricing
  get 'info' => "home#info", as: :info
  get 'pick_tune' => "tunes#pick", as: :pick_tune
  get 'parts/:id/add_favorite' => "parts#add_favorite", as: :add_favorite_part
  get 'parts/:id/remove_favorite' => "parts#remove_favorite", as: :remove_favorite_part
  post 'users/:id/select' => "users#select", as: :select_user
  post 'users/:id/take_over' => "users#take_over", as: :take_over_user
  post 'users/take_back' => "users#take_back", as: :take_back_user
  get 'tags/:tag', to: 'tunes#index', as: :tag
  put 'parts/update_hittimes/:id', to: 'parts#update_hittimes', as: :update_hittimes
  post 'parts/update_note_marks/:id', to: 'parts#update_note_marks', as: :update_note_marks
  delete 'annotations/delete_all_for_part/:part_id', to: 'annotations#destroy_all_for_part'
  get 'offline_favorites', to: 'favorites#offline_favorites'
  get 'offline_parts/:id', to: 'parts#offline'
  get 'serviceWorker.js', to: 'service_worker#index'
  get 'convert_to_scan/:id', to: 'parts#convert_to_scan'
  get 'groups/add_user/:id', to: 'groups#add_user', as: :group_add_user
  get 'groups/add_book/:id', to: 'groups#add_book', as: :group_add_book
  get 'timeline', to: 'recordings#current_user_timeline', as: :user_timeline
  get 'groups/:group_id/timeline', to: 'recordings#group_timeline', as: :group_timeline
  get 'students/:student_id/timeline', to: 'recordings#student_timeline', as: :student_timeline
  get 'parts/:id/assign_homework', to: 'parts#assign_homework', as: :assign_homework
  get 'parts/:id/remove_homework', to: 'parts#remove_homework', as: :remove_homework
  post 'questions/:id/check_answer', to: 'questions#check_answer'
  post 'comments/', to: 'comments#create'
  post 'parts/:id/youtube_links', to: 'parts#create_youtube_link', as: :youtube_links
  delete 'parts/:id/youtube_links/:youtube_link_id', to: 'parts#delete_youtube_link', as: :delete_youtube_link
  get 'questions/tagged_with/:tag', to: 'questions#tagged_with', as: :questions_tagged_with
  get 'parts/tagged_with/:tag', to: 'parts#tagged_with', as: :parts_tagged_with
  get 'part/:id/recordings', to: 'recordings#index', as: :part_recordings



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
