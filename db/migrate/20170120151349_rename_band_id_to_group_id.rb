class RenameBandIdToGroupId < ActiveRecord::Migration
  def change
    rename_column :users, :band_id, :group_id
    rename_column :tunes, :band_id, :group_id
    rename_column :books, :band_id, :group_id
  end
end
