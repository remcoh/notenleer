class AddBandIdToTune < ActiveRecord::Migration
  def change
    add_column :tunes, :band_id, :integer
  end
end
