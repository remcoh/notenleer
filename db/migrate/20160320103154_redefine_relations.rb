class RedefineRelations < ActiveRecord::Migration
  def change
    add_column :recordings, :part_id, :integer
    add_column :favorites, :part_id, :integer
    add_column :homeworks, :part_id, :integer
    add_column :user_logs, :part_id, :integer

    remove_column :recordings, :tune_id, :integer
    remove_column :favorites, :tune_id, :integer
    remove_column :homeworks, :tune_id, :integer
    remove_column :user_logs, :tune_id, :integer
  end
end
