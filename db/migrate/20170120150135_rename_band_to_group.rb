class RenameBandToGroup < ActiveRecord::Migration
  def change
    rename_table 'bands',  'groups'
  end
end
