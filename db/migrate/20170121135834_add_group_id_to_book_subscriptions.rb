class AddGroupIdToBookSubscriptions < ActiveRecord::Migration
  def change
    add_column :book_subscriptions, :group_id, :integer
  end
end
