class AddTuneFieldsToTune < ActiveRecord::Migration
  def change
    add_column :tunes, :title, :string
    add_column :tunes, :notation, :text
    add_column :tunes, :tempo, :integer
    add_column :tunes, :instrument, :string
  end
end
