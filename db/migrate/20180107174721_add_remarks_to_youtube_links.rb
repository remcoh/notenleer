class AddRemarksToYoutubeLinks < ActiveRecord::Migration
  def change
    add_column :youtube_links, :remarks, :text
  end
end
