class AddBandIdToBook < ActiveRecord::Migration
  def change
    add_column :books, :band_id, :integer
  end
end
