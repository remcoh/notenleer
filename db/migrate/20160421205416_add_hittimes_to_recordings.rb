class AddHittimesToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :hit_times, :string
  end
end
