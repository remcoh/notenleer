class AddPointsAndStatusToMedal < ActiveRecord::Migration
  def change
    add_column :medals, :points, :integer
    add_column :medals, :status, :string
  end
end
