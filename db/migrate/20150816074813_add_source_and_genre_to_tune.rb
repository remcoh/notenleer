class AddSourceAndGenreToTune < ActiveRecord::Migration
    def change
      add_column :tunes, :source, :string
      add_column :tunes, :genre, :string
    end
end
