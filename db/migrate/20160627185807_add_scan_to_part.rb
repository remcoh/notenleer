class AddScanToPart < ActiveRecord::Migration
  def change
    add_column :parts, :scan, :string
    add_column :parts, :part_type, :string
  end
end
