class AddAuthorToTune < ActiveRecord::Migration
  def change
    add_column :tunes, :author_id, :integer
  end
end
