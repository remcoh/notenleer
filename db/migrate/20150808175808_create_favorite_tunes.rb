class CreateFavoriteTunes < ActiveRecord::Migration
  def change
    create_table :favorite_tunes do |t|
      t.integer :user_id
      t.integer :tune_id

      t.timestamps null: false
    end
  end
end
