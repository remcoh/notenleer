class CreateAnnotations < ActiveRecord::Migration
  def change
    create_table :annotations do |t|
      t.integer :part_id
      t.string :text
      t.integer :xpos
      t.integer :ypos
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
