class AddAccessToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :access, :string
  end
end
