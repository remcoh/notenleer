class AddImageToBook < ActiveRecord::Migration
  def change
    add_column :books, :image, :string
    remove_column :books, :background_color
    remove_column :books, :title_color
  end
end
