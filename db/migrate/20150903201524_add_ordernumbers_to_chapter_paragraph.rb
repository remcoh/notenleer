class AddOrdernumbersToChapterParagraph < ActiveRecord::Migration
  def change
    rename_column :chapters, :page, :chapter_nr
    add_column :paragraphs, :paragraph_nr, :integer
    add_column :paragraphs, :title, :string
  end
end
