class AddColorsToBook < ActiveRecord::Migration
  def change
    add_column :books, :background_color, :string
    add_column :books, :title_color, :string
  end
end
