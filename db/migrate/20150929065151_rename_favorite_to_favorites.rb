class RenameFavoriteToFavorites < ActiveRecord::Migration
  def change
    rename_table :favorite, :favorites

  end
end
