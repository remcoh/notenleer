class AddTeacherInstrumentsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :teacher_instruments, :string
  end
end
