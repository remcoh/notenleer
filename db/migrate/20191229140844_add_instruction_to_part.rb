class AddInstructionToPart < ActiveRecord::Migration[5.2]
  def change
    add_column :parts, :instruction, :text
  end
end
