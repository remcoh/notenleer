class AddTimestampsToRecording < ActiveRecord::Migration
  def change
    add_column :recordings, :created_at, :timestamp
    add_column :recordings, :updated_at, :timestamp
  end
end
