class AddPartNumberToPartAndUser < ActiveRecord::Migration
  def change
    add_column :users, :part_number, :integer
    add_column :parts, :part_number, :integer
  end
end
