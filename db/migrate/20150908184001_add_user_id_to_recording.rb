class AddUserIdToRecording < ActiveRecord::Migration
  def change
    add_column :recordings, :user_id, :integer
    remove_column :recordings, :data
  end
end
