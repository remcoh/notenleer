class AddMultipleScansToPart < ActiveRecord::Migration
  def change
    rename_column :parts, :scan, :scan1
    add_column :parts, :scan2, :string
    add_column :parts, :scan3, :string
    add_column :parts, :scan4, :string
  end
end
