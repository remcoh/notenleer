class AddSoundtrackToRecording < ActiveRecord::Migration
  def change
    add_column :recordings, :soundtrack, :string
  end
end
