class AddHittimesToParts < ActiveRecord::Migration
  def change
    remove_column :recordings, :hit_times, :string
    add_column :parts, :hit_times, :string
  end
end
