class AddActivityToUserLog < ActiveRecord::Migration
  def change
    add_column :user_logs, :activity, :string
  end
end
