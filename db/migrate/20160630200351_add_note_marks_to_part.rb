class AddNoteMarksToPart < ActiveRecord::Migration
  def change
    add_column :parts, :note_marks, :text
  end
end
