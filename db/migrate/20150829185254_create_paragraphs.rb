class CreateParagraphs < ActiveRecord::Migration
  def change
    create_table :paragraphs do |t|
      t.integer :page_id
      t.string :content_type
      t.text :content
      t.timestamps null: false
    end
  end
end
