class AddCollectionToTune < ActiveRecord::Migration
  def change
    add_column :tunes, :collection, :string
  end
end
