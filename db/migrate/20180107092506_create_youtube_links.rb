class CreateYoutubeLinks < ActiveRecord::Migration
  def change
    create_table :youtube_links do |t|
      t.integer :tune_id
      t.string :youtube_url

      t.timestamps null: false
    end
  end
end
