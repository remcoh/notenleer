class AddInstrumentToBooks < ActiveRecord::Migration
  def change
    add_column :books, :instrument, :string
  end
end
