class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :chapter_id
      t.string :text
      t.string :type
      t.string :abc1
      t.string :abc2
      t.string :sound1
      t.string :sound2
      t.string :answer

      t.timestamps null: false
    end
  end
end
