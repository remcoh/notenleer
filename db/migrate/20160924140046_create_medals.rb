class CreateMedals < ActiveRecord::Migration
  def change
    create_table :medals do |t|
      t.integer :user_id
      t.string :color
      t.string :name
      t.string :image

      t.timestamps null: false
    end
  end
end
