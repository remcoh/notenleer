class CreateParts < ActiveRecord::Migration
  def change
    create_table :parts do |t|
      t.integer :tune_id
      t.text :notation
      t.string :instrument
      t.integer :author_id
      t.string :key

      t.timestamps null: false
    end
  end
end
