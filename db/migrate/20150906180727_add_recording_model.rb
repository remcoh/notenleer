class AddRecordingModel < ActiveRecord::Migration
  def change
    create_table :recordings do |t|
      t.integer :tune_id
      t.binary :data
    end

  end
end
