class AddMultipartToTune < ActiveRecord::Migration
  def change
    add_column :tunes, :multipart, :boolean
  end
end
