class RenameTuneUserConnectionsToFavorites < ActiveRecord::Migration
  def change
    remove_column :tune_user_connections, :connection_type
    rename_table :tune_user_connections, :favorite
  end
end
