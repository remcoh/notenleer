class RenameFavoriteToConnection < ActiveRecord::Migration
  def change
    rename_table "favorite_tunes", "homeworks"
    add_column :tune_user_connections, :connection_type, :string
  end
end
