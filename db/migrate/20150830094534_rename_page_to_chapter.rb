class RenamePageToChapter < ActiveRecord::Migration
  def change
    rename_table :pages, :chapters
  end

end
