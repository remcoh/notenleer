class AddSpeedVersionsToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :speed_versions, :string
  end
end
