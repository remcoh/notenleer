class CreateHomeworks < ActiveRecord::Migration
  def change
    create_table :homeworks do |t|
      t.integer :tune_id
      t.integer :chapter_id
      t.integer :user_id
      t.datetime :due_date
      t.integer :teacher_id
      t.text :remarks
      t.timestamps null: false
    end
  end
end
