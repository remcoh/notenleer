class RenamePageIdToChapterId < ActiveRecord::Migration
  def change
    rename_column :paragraphs, :page_id, :chapter_id
  end
end
