class AddRemarksToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :remarks, :string
  end
end
