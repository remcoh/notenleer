class AddHeaderFieldsToTune < ActiveRecord::Migration
  def change
    add_column :tunes, :meter, :string
    add_column :tunes, :key, :string
  end
end
