class AddStudyTrackToRecordings < ActiveRecord::Migration
  def change
    add_column :recordings, :study_track, :boolean
  end
end
