class AddParentIdToChapter < ActiveRecord::Migration
  def change
    add_column :chapters, :parent_id, :string
  end
end
