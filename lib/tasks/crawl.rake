def get_tune(tune_link)
  begin
    tune_page = tune_link.click
    abc = tune_page.search("textarea").text
    puts abc
    Tune.create_from_abc(abc)
  rescue Exception => e
   puts "err0r #{e.to_s}"
  end
end

namespace :crawl do
  desc 'craw abctunes.com'
  task abctunes: :environment do
    tunecrawler = TuneCrawler.new
    tunecrawler.crawl
  end

  task remove_trailing_newline: :environment do
    for tune in Tune.all
      unless tune.notation.nil?
        slicedtune = tune.notation.dup
        slicedtune.gsub!(/.*?(?=X:)/im, "")
        tune.notation = slicedtune
        tune.save
      end
    end
  end

end
