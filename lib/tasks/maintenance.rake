namespace :maintenance do
  task create_parts: :environment do
    Tune.all.each do |tune|
      begin
        puts "create part"
        Part.create tune_id: tune.id, notation: tune.notation, key: tune.key, instrument: tune.instrument
      rescue Exception => e
        puts e
      end
    end

  end

end