module Helpers
  module SessionHelpers

    def create_teacher_student
      group = Group.create name: 'Testmuziekschool'

      teacher = User.create(email: 'leraar@onzemuziek.nl',
                            password: 'test1234',
                            password_confirmation: 'test1234',
                            role: 'teacher',
                            first_name: 'Leeraarvoornaam',
                            last_name: 'Leeraarachternaam',
                            instrument: 'trompet',
                            group_id: group.id,
                            confirmed_at: DateTime.now

      )
      student = User.create(email: 'student@onzemuziek.nl',
                            password: 'test1234',
                            password_confirmation: 'test1234',
                            role: 'student',
                            first_name: 'Studentvoornaam',
                            last_name: 'Studentachternaam',
                            instrument: 'trompet',
                            teacher_id: teacher.id,
                            group_id: group.id,
                            confirmed_at: DateTime.now
      )
      #teacher.confirm!
      #student.confirm!
    end


    def sign_in(email, password)
      visit root_path
      fill_in 'Email', with: email
      fill_in 'Password', with: password
      click_button 'Inloggen'
      save_and_open_page

    end
  end
end