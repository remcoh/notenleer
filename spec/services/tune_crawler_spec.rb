require 'rails_helper'

RSpec.describe TuneCrawler do

  it 'should respond to new' do
    expect(TuneCrawler).to respond_to(:new)
  end

  describe '#crawl' do

    subject { TuneCrawler.new('http://abcnotation.com/search') }

    it "should call the indexpage for the letter all the letters in the alfabeth" do
      expect(subject).to receive(:get_index_page).with(Mechanize::Page::Link).exactly(26).times
      subject.crawl
    end

  end

  describe '#get_all_songs' do

    subject { TuneCrawler.new('http://abcnotation.com/search') }

    it "should fetch all the songs on the current page"

    it "should press next after the processing of the songs if next is available"

  end

end