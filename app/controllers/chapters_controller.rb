class ChaptersController < ApplicationController
  before_action :authenticate_user!
  before_action :find_book
  before_action :find_chapter, except:[:new, :create]

  def show
    @breadcrumbs = 
      [
        { title: "boeken", link: books_url },
        { title: @book.title, link: book_url(@book) }
      ]
    @chapter = Chapter.find(params[:id])
  end

  def new
    if params.keys.include?('parent_id')
      @parent = Chapter.find(params[:parent_id])
      @chapter = @parent.children.create book_id: params[:book_id], chapter_nr: (@book.chapters.where(parent_id: params[:parent_id]).
          order(:chapter_nr).last.try(:chapter_nr)|| 0) + 1 , content: ""
    else
      @chapter = Chapter.new book_id: params[:book_id], chapter_nr: (@book.chapters.where(parent_id: nil).order(:chapter_nr).last.try(:chapter_nr) || 0) + 1
    end
  end

  def update
    params.permit!
    @chapter.update_attributes params[:chapter]
    redirect_to book_chapter_url @book, @chapter
  end

  def create
    params.permit!
    @chapter = Chapter.create(params[:chapter].merge(book_id: @book.id))
    redirect_to book_chapter_url @book, @chapter
  end

  def destroy
    Chapter.find(params[:id]).destroy
    redirect_to book_url(@book)
  end

  private

  def find_book
    @book = Book.find(params[:book_id])
  end

  def find_chapter
    @chapter = Chapter.find(params[:id])
  end

end
