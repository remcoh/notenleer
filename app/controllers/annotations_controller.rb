class AnnotationsController < ApplicationController
  before_action :authenticate_user!
  def index
    Annotation.where(part_id: params[:part_id])
    render json: {result: 'ok'}
  end

  def create
    params.permit!
    Annotation.create part_id: params[:part_id],
                      user_id: params[:user_id],
                      text: params[:text],
                      xpos: params[:xpos],
                      ypos: params[:ypos]

    render json: {result: 'ok'}
  end

  def destroy
    Annotation.destroy params[:id]
  end

  def destroy_all_for_part
    Annotation.destroy_all part_id: params[:part_id], user_id: params[:user_id]
    render json: {result: 'ok'}
  end

end
