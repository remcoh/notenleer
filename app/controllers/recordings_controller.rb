class RecordingsController < ApplicationController
  before_action :authenticate_user!

  def show

  end

  def index
    @part = Part.find(params[:id])
    @recordings = @part.recordings.paginate(per_page: 20, page: params[:page])
    respond_to do |format|
      format.js
    end
  end

  def create
    params.permit!
    if params[:audio]
      find_path_and_tune
      @recording = Recording.create(user_id: params[:user_id], part_id: params[:part_id], remarks: params[:remarks], access: params[:access])
      audio = params[:audio]
      audio.rewind
      File.open("#{save_path}.wav", 'wb') do |f|
        f.write params[:audio].read
      end
      %x[ffmpeg -i #{save_path}.wav #{save_path}.mp3 && rm -rf #{save_path}.wav}]
      %x[rm -rf #{save_path}.wav]
      @recording.update_attribute :soundtrack, "#{@part.id},mp3"
    elsif params[:recording]
      find_path_and_tune
      @recording = Recording.create(params[:recording])
      filename = @recording.soundtrack_base_file_name
      %x[ffmpeg -i public/audios/#{filename}.mp3 public/audios/#{filename}.wav]
      @recording.speed_factors.each do |factor|
        %x[rubberband public/audios/#{filename}.wav public/audios/#{filename}_#{factor}.wav -T 0.#{factor}]
        %x[ffmpeg -i public/audios/#{filename}_#{factor}.wav public/audios/#{filename}_#{factor}.mp3]
        %x[rm -rf public/audios/#{filename}_#{factor}.wav]
      end
      %x[rm -rf public/audios/#{filename}.wav]
    end
    @recordings = Recording.where(part_id: params[:part_id]).paginate(page: params[:page] || 1, per_page: 40)
    UserMailer.notify_upload(@recording).deliver_now unless @recording.user.teacher == nil
    respond_to do |format|
      format.html { redirect_to @part }
      format.js
    end
  end

  def update
    @recording = Recording.find(params[:id])
    respond_to do |format|
      format.json {
        render json: {result: "ok"}
      }
    end
  end

  def destroy
    @part = Part.find(params[:part_id])
    @recording = Recording.find(params[:id]).destroy
    @recordings = Recording.where(part_id: params[:part_id]).paginate(page: params[:page] || 1, per_page: 40)
    if @recording.soundtrack.file.nil? #recorded file
      %x[rm -rf #{save_path}.mp3]
    else #uploaded file
      @recording.speed_factors.each do |factor|
        %x[rm -rf public/audios/#{@recording.soundtrack_base_file_name}_#{factor}.mp3]
      end
    end

    redirect_to part_path @part
  end

  def current_user_timeline
    breadcrumbs
    @timeline_title = current_user.name
    @recordings = current_user.recordings.order('created_at desc').joins(:part).joins('INNER JOIN tunes on tunes.id = parts.tune_id')
    render 'recordings/timeline'
  end

  def group_timeline
    breadcrumbs
    @timeline_title = current_user.group.name
    @recordings = current_user.group.recordings(current_user)
    render 'recordings/timeline'
  end

  def student_timeline
    breadcrumbs
    student = User.find(params[:student_id])
    @timeline_title = student.name
    @recordings = current_user.student_recordings(student)
    render 'recordings/timeline'
  end


  private

  def find_path_and_tune
    @part = Part.find(params[:part_id])
    @tune = @part.tune
  end

  def save_path
    Rails.root.join("public/audios/#{@recording.id}")
  end

  def breadcrumbs
    @breadcrumbs =
        [
            { title: "books", link: root_url }
        ]
  end

end
