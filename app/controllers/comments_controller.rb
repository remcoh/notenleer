class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @comment = Comment.create(text: params[:text],
                   commentable_type: params[:commentable_type],
                   commentable_id: params[:commentable_id],
                   user_id: current_user.id
    )
    @comments = @comment.commentable.comments
    UserMailer.notify_comment(@comment, current_user).deliver_now

    respond_to do |format|
      format.html
      format.js
    end
  end

end