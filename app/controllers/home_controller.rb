class HomeController < ApplicationController
  layout 'home'
  def index
  end

  def user_settings

  end

  def user_list
    @users = User.all
  end
end
