class QuestionsController < ApplicationController
  before_action :authenticate_user!

  def index
    scope = Question
    scope = scope.tagged_with(params[:tag_search]) if params.include?(:tag_search)


    @filterrific = initialize_filterrific(
      Question,
      params[:filterrific],
      select_options: {
      },
      persistence_id: false,
      default_filter_params: {sorted_by: 'Title'},
      available_filters: [:upcoming, :description_filter, :list_filter],
      sanitize_params: true,
    ) || return
    @questions = @filterrific.find.page(params[:page])
    
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @question = Question.find params[:id]
    if params['set']
      ids = params[:set].split(',')
      @questions = Question.where(id: ids)
    else
      @questions = [Question.find(params[:id])]
    end  
    @question_count = @questions.length
  end

  def tagged_with
    @questions = Question.tagged_with params[:tag]
    @question_count = @questions.length
    render action: :show
  end

  def new
    @question = Question.create(type: 'Open')
    redirect_to edit_question_url @question
  end

  def edit
    @question = Question.find(params[:id])
  end

  def update
    params.permit!
    @question = Question.find(params[:id])
    @question.update_attributes params[:question]
    redirect_to questions_url
  end

  def destroy
    @question = Question.find(params[:id])
    @question.delete
    redirect_to questions_url
  end

  def check_answer
    @question = Question.find(params[:id])
    result = params[:answer].downcase == @question.answer.downcase
    render js: "maestoso.answer.processAnswer(#{params[:id]}, #{result})"
  end

end