class UserLogsController < ApplicationController
  before_action :authenticate_user!

  def show
  	@user_log = UserLog.find params[:id]
  	respond_to do |format|
      format.js
    end
  end

end
