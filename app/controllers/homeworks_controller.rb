class HomeworksController < ApplicationController
  before_action :authenticate_user!
  def index
    @homeworks = current_user.homeworks.order('created_at desc')
  end

  # def create
  #   Homework.create user_id: session[:current_student]["id"], part_id: params[:part_id], remarks: params[:homework][:remarks], due_date: params[:homework][:due_date]
  #   @student = User.find(session[:current_student]["id"])
  #   @tune = Part.find(params[:part_id]).tune
  #   UserMailer.notify_homework(current_user, @student, @tune).deliver_now
  #   redirect_to part_path(params[:part_id])
  # end

end
