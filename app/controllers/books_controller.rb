class BooksController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user_var


  def show
    @book = Book.find(params[:id])
    @breadcrumbs = [{ title: "boeken", link: books_url }]
    render :show
  end

  def new
    @book = Book.new
  end

  def index
    user_books = Book.for_user(current_user)
    group_books = Book.where(group_id: current_user.group_id).all
    @instrument_books = {}
    if ['admin', 'teacher'].include?(current_user.role)
      Settings.instruments.each do |i|
        books = Book.where(instrument: i).all
        @instrument_books[i] = books unless books.empty?
      end
    end
    @books = (user_books + group_books).uniq

  end

  def create
    params.permit!
    @book = Book.new(params[:book])
    @book.update_attribute :author_id, current_user.id

    redirect_to book_url(@book)
  end

  def edit
    @book = Book.find(params[:id])
    redirect_to "/" and return unless current_user.may_update(@book)
  end

  def update
    @book = Book.find(params[:id])
    redirect_to "/" and return unless current_user.may_update(@book)
    params.permit!
    @book.update_attributes params[:book]
    redirect_to books_url @book
  end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    redirect_to books_url
  end

  private

  def set_user_var
    @user = current_user
  end

end
