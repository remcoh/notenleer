class TunesController < ApplicationController
  include TunesHelper
  before_action :authenticate_user!, except: [:practice, :pick, :show]
  before_action :find_tune, only: [:show, :edit, :update, :destroy, :add_favorite, :remove_favorite]

  def index
    if params[:list] == 'favorites'
      scope = Tune.for_user(current_user)
    elsif params[:list] == 'own'
      scope = Tune.authored_by(current_user)
    elsif ["admin", "teacher"].include?(current_user.role)
      scope = Tune.for_user(current_user)
    else
      scope = Tune.for_user(current_user)
    end
    #scope = scope.tagged_with(params[:tag_search]) if params.include?(:tag_search)   
    @filterrific = initialize_filterrific(
        Tune,
        params[:filterrific],
        select_options: {
        },
        persistence_id: false,
        default_filter_params: {sorted_by: 'Title'},
        available_filters: [:upcoming, :description_filter, :list_filter],
        sanitize_params: true,
    ) || return
    @tunes = @filterrific.find.page(params[:page])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    redirect_to part_url(@tune.part_for_user(current_user), tags: params[:tags], index: params[:index], chapter_id: params[:chapter_id])
  end

  def new
    @tune = Tune.create(author_id: current_user.id,
                        title: params[:title] || "new song",
                        meter: "4/4",
                        tempo: 60,
                        group_id: params[:group_id],
                        multipart: params[:multipart]
    )
    @part = Part.create(notation: "X:001&#13T:#{params[:title] || "new song"}&#13M:#{params[:meter]}&#13K:#{params[:key]||"C"}&#13L:1/8&#13Q:60&#13P:#{params[:instrument]}&#13|".html_safe,
                        author_id: current_user.id,
                        tune_id: @tune.id,
                        instrument: params[:instrument],
                        part_type: params[:part_type] || 'abc'
    )
    redirect_to part_path @part, state: :edit
  end

  def update
    render json: {result: "not authorized"} and return unless current_user.may_update(@tune)
    params.permit!
    @tune.update_attributes(params[:tune])
    respond_to do |format|
      format.json {
        render json: {result: "ok"}
      }
    end
  end

  def destroy
    @tune.destroy
    redirect_to tunes_path list: params[:list]
  end

  def pick
    tune_collection = current_user.nil? ? Tune.for_anonymous : Tune
    if params[:meter]
      tunes = tune_collection.with_meter(params[:meter])
    else
      tunes = tune_collection.all
    end
    @tune = tunes.offset(rand(tunes.count)).first
    unless @tune.nil?
      redirect_to tune_path(@tune, meter: params[:meter])
    else
      flash[:notice] = "No song found, try to refine the search"
      redirect_to "/practice"
    end
  end

  def add_favorite
    @tune.favorites.build(user_id: params[:user_id]).save!
    redirect_to tune_path(@tune)
  end

  def remove_favorite
    @tune.favorites.where(user_id: params[:user_id]).delete_all
    redirect_to tune_path(@tune)
  end

  private

  def find_tune
    @tune = Tune.find(params[:id])
  end

end
