class MessagesController < ApplicationController
  before_action :authenticate_user!
  def create
    params.permit!
    @part = Part.find(params[:message][:part_id])
    @message = Message.create(params[:message])
    UserMailer.share_email(@message).deliver_now
    respond_to do |format|
      format.html
      format.js
    end
  end

  def destroy
    Message.destroy params[:id]
    redirect_to :back
  end

end
