class GroovesController < ApplicationController
    before_action :authenticate_user!
  
    def index
      scope = Groove
      @filterrific = initialize_filterrific(
        Groove,
        params[:filterrific],
        sanitize_params: true,
      ) || return
      @grooves = @filterrific.find.page(params[:page])
      
      respond_to do |format|
        format.html
        format.js
      end
    end
  
    def show
      @groove = Groove.find params[:id]
    end
  
  
    def new
      @groove = Groove.new
    end
  
    def edit
      @groove = Groove.find(params[:id])
    end

    def create
      params.permit!
      @groove  = Groove.new(params[:groove])
      @groove.save
      redirect_to groofe_url @groove
    end
    
    def update
      params.permit!
      @groove = Groove.find(params[:id])
      @groove.update_attributes params[:groove]
      redirect_to groofe_url @groove
    end
  
    def destroy
      @groove = Groove.find(params[:id])
      @groove.delete
      redirect_to grooves_url
    end  
  end