class PartsController < ApplicationController
  include TunesHelper
  #before_action :authenticate_user!
  before_action :offline_login, :find_part, only: [:show, :edit, :update, :destroy, :add_favorite, :remove_favorite, :create_youtube_link, :delete_youtube_link]
  def show
    @tune = @part.tune
    @parts = [@part]
    @partscount = @tune.parts.count
    @other_parts = @tune.parts.where("id != #{@part.id}")
    @tools = params[:tools] ? params[:tools].split('-') : []
    find_groove_url
    find_breadcrumbs
    if params[:layout] == "print"
      render template: "parts/print"
    else
      render
    end
  end

  def tagged_with
    @parts = Tune.tagged_with(params[:tag]).collect{|t| t.parts.all}.flatten
    @partscount = @parts.length
    @part = @parts.first
    @tune = @part.tune
    @tools = params[:tools].split('-')
    find_groove_url
    render action: :show
  end

  def new
    params.permit!
    @tune = Tune.find(params[:tune_id])
    @part = Part.create(notation: "X:001&#13T:#{@tune.title}&#13M:#{params[:meter] || @tune.meter}&#13K:#{params[:key]||"C"}&#13L:1/8&#13Q:#{@tune.tempo}&#13P:#{params[:instrument]}&#13|".html_safe,
                        author_id: current_user.id,
                        tune_id: @tune.id,
                        instrument: params[:instrument],
                        part_number: params[:part_number],
                        key: params[:key],
                        part_type: params[:part_type] || 'abc'
    )
    redirect_to part_path @part, state: :edit
  end

  def update
    params.permit!
    @tune = @part.tune
    may_update = current_user.may_update(@tune)
    if may_update
      if @part.part_type == 'abc'
        @part.update_attributes({key:  /K: *(.*)\r/.match(params[:part][:notation])[1],
                             instrument: params[:part][:instrument],
                             part_number: params[:part][:part_number],
                             notation: (params[:part][:notation]),
                             part_type: params[:part_type] || 'abc',
                             instruction: params[:part][:instruction]
                            })
        @tune.update_attributes(params[:tune].merge(meter: /M: *(.*)\r/.match(params[:part][:notation])[1], tempo: /Q: *(.*)\r/.match(params[:part][:notation])[1]))
      elsif ['scan', 'pdf'].include? @part.part_type
        # we assume that when the first file is a pdf, all files are!
        part_type = params[:part][:scan1] && params[:part][:scan1].content_type == "application/pdf" ?  'pdf' : 'scan'
        @part.part_type = part_type
        @part.update_attributes params[:part].merge(part_type: part_type)
      elsif @part.part_type == 'drum'
          @part.update_attribute :notation, params[:drumtabs]
      end

    end
    respond_to do |format|
      format.json {
        render json: {result: "ok"}
      }
      format.html {
        redirect_to part_url @part
      }
    end
  end

  def update_hittimes
    @part =Part.find params[:id]
    @part.update_attribute :hit_times, params[:hit_times]
  end

  def update_note_marks
    @part =Part.find params[:id]
    @part.update_attribute :note_marks, params[:note_marks]
    render json: {result: "ok"}
  end

  def add_favorite
    @part.favorites.build(user_id: params[:user_id]).save!
    redirect_to part_path(@part)
  end

  def remove_favorite
    @part.favorites.where(user_id: params[:user_id]).delete_all
    redirect_to part_path(@part)
  end

  def destroy
    @tune = @part.tune
    @part.destroy
    if @tune.parts.count > 0
      redirect_to @tune.parts.first
    else
      redirect_to tunes_path list: params[:list]
    end
  end

  def convert_to_scan
    @part = Part.find(params[:id])
    @part.update_attribute :part_type, 'scan'
    redirect_to part_url(@part)
  end

  def assign_homework
    @part = Part.find(params[:id])
    Homework.create user_id: session[:current_student]['id'], part_id: @part.id
    @student = User.find(session[:current_student]['id'])
    @tune = @part.tune
    UserMailer.notify_homework(current_user, @student, @tune).deliver_now

    redirect_to part_url(@part)
  end

  def remove_homework
    @part = Part.find(params[:id])
    Homework.delete_all(['part_id = ? and user_id = ?', @part.id, session[:current_student]['id']])
    redirect_to part_url(@part)
  end

  def create_youtube_link
    params.permit!
    @part.tune.youtube_links.create(params[:youtube_link])
    redirect_to part_url @part
  end

  def delete_youtube_link
    YoutubeLink.delete(params[:youtube_link_id])
    redirect_to part_url @part
  end

  private

  def is_homework_for(user)
    return false if session[:current_student].nil?
    Homework.where(['part_id = ? and user_id = ?', @part.id, session[:current_student]['id']]).length > 0
  end

  def find_part
    @part = Part.find(params[:id])
  end
  
  def find_breadcrumbs
    if params.keys.include?("chapter_id")
      @chapter = Chapter.find(params[:chapter_id])
      @book = @chapter.book
      @breadcrumbs = 
        [
          { title: "boeken", link: books_url },
          { title: @book.title, link: book_url(@book) },
          { title: @chapter.title, link: book_chapter_url(@book, @chapter) }
        ]
    elsif
      params.keys.include?("favorites")
      @breadcrumbs = 
        [
          { title: "Favorieten", link: favorites_url }
        ]
     end  
       
  end

  def get_annotations(part)
    return [] if session['current_student'].nil?
    part.annotations_for(session['current_student']['id'])
  end

  def offline_login
    sign_in(User.first) if params[:offline] == 'true'
  end

  def find_groove_url
    groove_info = @tools.find { |e| /^groove/ =~ e }
    return unless groove_info 
    groove_info_arr = groove_info.split(':')
    @groove_url = Groove.find(groove_info_arr[1]).url.gsub(/Tempo=\d+/, "Tempo=#{groove_info_arr[2]}")
  end  
  
end
