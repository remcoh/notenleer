class BookSubscriptionsController < ApplicationController
  before_action :authenticate_user!

  def new
    @book_subscription = BookSubscription.new
  end

  def create
    params.permit!
    BookSubscription.create params[:book_subscription]
    redirect_to user_url(params[:book_subscription][:user_id])
  end

  def destroy
    @book_subscription = BookSubscription.find(params[:id])
    @book_subscription.destroy
    redirect_to user_url @book_subscription.user_id
  end

end
