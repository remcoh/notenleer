class UsersController < ApplicationController
  before_action :authenticate_user!, except: [:new, :create]

  def index
    if current_user.role == "teacher"
      scope = User.where teacher_id: current_user
    elsif current_user.role == "admin"
      unless params["role"].nil? || params[:role] == ""
        scope = User.where(role: params[:role])
      else
        scope = User
      end
    else
      scope = User.nil
    end
    puts "scope: count=#{scope.count}"
    #@filterrific = initialize_filterrific(scope,params[:filterrific]) or return
    #@users = @filterrific.find.page(params[:page])
    @filterrific = initialize_filterrific(scope.order(:last_name),params[:filterrific]) or return
    @users = @filterrific.find.page(params[:page])

  end

  def show
   @user = User.find params[:id]
   if current_user.role == "admin" || (current_user.role == "teacher" && @user.teacher_id == current_user.id)
     @subscriptions = @user.book_subscriptions.paginate(page: params[:page], per_page: 40)
     @homeworks = Homework.for_user(@user).paginate(page: params[:page], per_page: 40)
     @recordings = Recording.for_user(@user).where('study_track is not true').order('created_at desc').paginate(page: params[:page], per_page: 40)
     @user_logs = @user.user_logs
     @users = @user.students if current_user.role == "admin"
   else
     render :text => "No access..."
   end

  end

  def new
    @user = User.new group_id: params[:group_id]
  end

  def edit
    @user = User.find params[:id]
  end

  def update
    @user = User.find params[:id]
    @user.update_attributes params[:user].permit!
    redirect_to users_url(@user, role: params[:role])
  end

  def create
    # if a teacher creates a user he/she is the users teacher
    @user = User.create(email: params[:user][:email],
                        password: params[:user][:password],
                        password_confirmation: params[:user][:password_confirmation],
                        first_name: params[:user][:first_name],
                        last_name: params[:user][:last_name],
                        instrument: params[:user][:instrument],
                        part_number: params[:user][:part_number],
                        role: set_role(params[:user][:role]),
                        teacher_id: set_teacher(params[:user][:teacher_id]),
                        group_id: set_group(params[:user][:group_id]))

    redirect_to users_url
  end

  def destroy
    User.find(params[:id]).destroy
    redirect_to users_url
  end

  def new_book_subscription
  end

  def select
    session[:current_student] = User.find(params[:id])
    redirect_to :users
  end

  def take_over
    @user = User.find(params[:id])
    if current_user.may_take_over(@user)
      session[:overtaking_user] = current_user
      sign_in(User.find(params[:id]))
    end
    redirect_to "/"
  end

  def take_back
    unless session[:overtaking_user].nil?
      sign_in(User.find(session[:overtaking_user]["id"]))
      session[:overtaking_user] = nil
    end
    redirect_to :users
  end

  private

  def set_role(role)
    if current_user && current_user.role == 'admin'
      role = role || "teacher"
    elsif current_user && current_user.role == 'teacher'
      role = 'student'
    else
      role = 'student'
    end
    return role
  end

  def set_teacher(teacher)
    if current_user && current_user.role == 'admin'
      teacher = teacher
    elsif current_user && current_user.role == 'teacher'
      teacher = current_user.id
    else
      teacher = nil
    end
    return teacher
  end

  def set_group(group)
    if current_user && current_user.role == 'admin'
      group = group
    elsif current_user && current_user.role == 'teacher'
      group = current_user.group.id
    else
      group = nil
    end
    return group
  end

end
