class GroupsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_group, only: [:show, :edit, :update, :destroy, :add_user, :add_book]

  def index
    @groups = Group.for_user(current_user).paginate(per_page: 20, page: params[:page])
  end

  def show
    @users = @group.users
    @books = @group.books
  end

  def new
    @group = Group.new
  end

  def edit
  end

  def update
    params.permit!
    if params.keys.include?('user_id')
      User.find(params[:user_id]).update_attribute :group_id, @group.id
    elsif params.keys.include?('book_id')
      BookSubscription.create book_id: params[:book_id], group_id: @group.id
    else
      @group.update_attributes params[:group]
    end
    redirect_to @group
  end

  def create
    params.permit!
    @group  = Group.new(params[:group])
    @group.owner_id = current_user.id
    @group.save
    redirect_to group_url @group
  end

  def destroy
    @group.destroy
    redirect_to groups_url
  end

  def add_user
  end

  def add_book
  end

  private

  def find_group
    @group = Group.find(params[:id])
  end

end
