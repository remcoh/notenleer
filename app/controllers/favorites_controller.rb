class FavoritesController < ApplicationController
  before_action :authenticate_user!, only: [:create]

  def index
    if current_user.nil?
      @offline = true
      @user = User.find(JsonWebToken.decode(params["t"])[:user_id])
    else
      authenticate_user!
      @user = current_user
    end
    @breadcrumbs = [{ title: "Boeken en collecties", link: books_url }]
    @parts = Part.joins(:favorites).joins(:tune).where(["favorites.user_id = ?", @user.id]).page(params[:page])
  end

  def create
    for user_id in params[:user_ids]
      TuneUserConnection.create user_id: user_id, connection_type: 'homework'
    end
    redirect_to tune_path(params[:tune_id])
  end

  def offline_favorites
    @user = User.find(params[:uid])
  end

end
