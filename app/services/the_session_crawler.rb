class TheSessionCrawler

  def initialize
    @mech     = Mechanize.new
    #@mech.set_proxy '145.53.198.29', 80
  end

  def crawl
    (0..1000).each do |i|
      begin
        url = "https://thesession.org/tune/#{i}/abc"
        puts " -------> Downloading #{url}"
        file = @mech.get url
        abc = file.body.split("\r\r\n")[0]
        if abc.match(/Q:/).nil?
          abc = abc.gsub(/X: *\d*/, "X:001\\nQ:60")
        end
        Tune.create_from_abc(abc)
        sleep 1
      rescue Exception => e
        puts e.to_s
      end
    end
  end

end
