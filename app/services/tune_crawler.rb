class TuneCrawler

  def initialize
    @mech     = Mechanize.new
    @mech.set_proxy '52.91.97.146', 8083
  end

  def crawl
    get_all_songs
  end

  def get_all_songs
    (22..25).each do |page|
      url = "http://abcnotation.com/browseTunes?n=#{("000"+page.to_s)[-4, 4]}"
      index_page = @mech.get url
      puts "going to index page: #{url}"
      get_songs_from_page(index_page)
    end

  end

  private

  def get_songs_from_page(page)
      tune_links = page.links.find_all { |l| l.href[0..8] == "/tunePage" if l.href }
      for tune_link in tune_links
        get_tune(tune_link)
        sleep 2
      end
  end

  def get_tune(tune_link)
    begin
      tune_page = tune_link.click
      abc = tune_page.search("textarea").text
      tune = Tune.create_from_abc(abc)
      puts tune.title
    rescue Exception => e
    puts "error #{e.to_s}"
    end
  end

end
