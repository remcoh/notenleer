module TunesHelper

  def tune_link(tune)
  	view_context.link_to(tune.title, tune_url(tune), target: "_new").html_safe
  end

  def part_link(part)
    view_context.link_to(part.title, part_url(part), target: "_new").html_safe
  end
end
