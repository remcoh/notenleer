module ChaptersHelper

  def openlink
    if %w(teacher admin).include?(current_user.role)
      "Open tune"
    else
      "Practice tune"
    end
  end

  def replace_content_placeholders(text)
    text.gsub(/tune\[(\d*)\]/) do |id|
      tune = Tune.find(id[5..-2].to_i)
      part = tune.part_for_user(current_user)
      out = ''
      out << "<div onclick='javascript:maestoso.part.open(#{part.id}, #{@chapter.id})' id='tune#{part.id}' class='abcTune ' data-abc='#{part.notation.gsub("\r\n", "&#xa;")}'></div>".html_safe if part.part_type == 'abc'
      out << "<div class='tune_controls'>"
      out << "<a onclick=\"maestoso.abcObjects['tune#{part.id}'].play(0, 1000)\" class='glyphicon glyphicon-volume-down play-inline-tune' ></a>"
      out << '&nbsp; &nbsp;'
      out << link_to("", part_url(part, chapter_id: @chapter.id), class: "glyphicon glyphicon-new-window")
      out << "</div>"

      out
    end.gsub(/partlink\[[^\[]*\]/) do |a|
      args = a[9..-2].split(',')
      part = Part.find(args[0])
      out = ''
      out << link_to(part.title, part_url(part, chapter_id: @chapter.id, tools: args[1].strip), class: 'link')
      out
    end.gsub(/tagged-partlinks\[[^\[]*\]/) do |tags|
      tunes = Tune.tagged_with(tags[17..-2])
      html = ""
      i = 1
      for tune in tunes
        html << "<div id='row'>"
        html << link_to(tune.title, tune_url(tune, index: i, tags: tags[1..-2], chapter_id: @chapter.id))
        html << "</div>"
        i += 1
      end
      html
    end.gsub(/tagged-parts\[[^\[]*\]/) do |a|
      args = a[13..-2].split(',')
      #arr = link_and_tag[13..-2].split(',')
      out = ''
      out << link_to(args[0], parts_tagged_with_url(args[1].strip, tools: args[2].strip), class: 'link')
      out
    end.gsub(/tagged-questions\[[^\[]*\]/) do |link_and_tag|
      arr = link_and_tag[17..-2].split(',')
      out = ''
      out << link_to(arr[0], questions_tagged_with_url(arr[1].strip), class: 'link')
      out
    end.gsub(/questions\[(.*)\]/) do |str|
      ids = str[10..-2].split(',')
      questions = Question.find(ids)
      out=""
      out << self.question(questions)
      out
    end.gsub(/questionlink\[(\d*)\]/) do |str|
      id = str[13..-2].to_i
      question = Question.find(id)
      out=""
      out << link_to(question.title, question_url(question, chapter_id: @chapter.id))
      out
    end.gsub(/reading-excercise-tags\[[^\[]*\]/) do |ex|
      out = ""
      tags_tempo = ex[23..-2].split(':')
      out << reading_excercise_tags(tags_tempo[0])
      out
    end.gsub(/reading-excercise-random\[[^\[]*\]/) do |ex|
      out = ""
      info = ex.gsub('<br>', '').gsub('&nbsp;', '')[25..-2].gsub("<p>", "").split("</p>").drop(1)
      out << reading_excercise_random(info[0],info[1], info[2], info[3], info[4..-1])
      out
    end
  end
end
