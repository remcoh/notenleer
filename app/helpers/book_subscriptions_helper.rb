module BookSubscriptionsHelper

  def unsubscribe(book, user)
    subscription = BookSubscription.where(user_id: user.id).where(book_id: book.id).first
    return "" if subscription.nil?
    link_to(t('book_unsubscribe'), book_subscription_url(subscription.id), method: :delete)
  end

end
