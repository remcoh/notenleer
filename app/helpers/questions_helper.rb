module QuestionsHelper
  def question(questions)
    out = ''
    hidden = false
    prev_question = nil
    questions.each do |question|
      next_question = questions[questions.index(question) + 1]
      out << "<div class='question_container #{hidden ? 'hidden' : ''}'>"
      out << "<span id='question_#{question.id}' class='question' data-question=''>#{question.text}"
      out << link_to("<span class='glyphicon glyphicon-edit' aria-hidden='true'></span>".html_safe, edit_question_url(question.id), remote: true, class: 'edit_question') if controller.controller_name == 'chapters'
      out << "</span>".html_safe
      out << self.send(question.type.underscore, question)
      out << "<div class = 'question-navigation'>"
      if prev_question
        out << "<a class = 'question-link' data-question=#{prev_question.id}><span class='glyphicon glyphicon-arrow-left' aria-hidden='true'></span> Vorige vraag</a>"
      end
      if next_question
        out << "<a class = 'question-link' data-question=#{next_question.id}>Volgende vraag <span class='glyphicon glyphicon-arrow-right' aria-hidden='true'></span></a>"
      end
      out << "</div>"
      out << "</div>"
      hidden  = true
      prev_question = question
    end
    out
  end

  def open(question)
    out = "<div>"
    out << "#{t('questions.answer')}: "
    out << "<input type='text' id='question_answer_#{question.id}'/>"
    out << "<a class='btn btn-info btn-check' data-question_id=#{question.id} data-questiontype='#{question.type}'>#{t('questions.check')}</a>"
    out << "<span id='question_result_#{question.id}' class='question_result glyphicon'></span>"
    out << "</div>"

  end

  def open_score(question)
    out = ""
    out << "<div id=question_#{question.id}_abc1 class = 'abcTune'  data-abc='#{question.abc1.gsub("\r\n", "&#xa;")}' data-abcmodel='true' >"
    out << "</div>"
    out << "#{t('questions.answer')}: "
    out << "<input type='text' id='question_answer_#{question.id}'/>"
    out << "<a class='btn btn-info btn-check' data-question_id=#{question.id} data-questiontype='#{question.type}'>#{t('questions.check')}</a>"
    out << "<span id='question_result_#{question.id}' class='question_result glyphicon'></span>"
  end

  def multiple_choice(question)
    out = "<span id='question_result_#{question.id}' class='question_result glyphicon'></span>"
    question.choices.split("\r\n").each do |c|
      out << "<div class='question_choice'>"
      out << radio_button_tag("question_answer_#{question.id}", c, false, class: "choose-multiple-choice", "data-question_id" => question.id )
      out << " #{c}"
      out << "</div>"
    end
    out
  end
end