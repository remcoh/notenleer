module ExcerciseHelper
  require 'digest/md5'
  def reading_excercise_tags(tags)
    out = ''
    exid = Time.now.to_f.to_s.gsub('.', '')
    out << "<div id= '#{exid}'>"
    tunes = Tune.tagged_with(tags)
    parts = []
    tunes.each do |t|
      part = t.part_for_user(current_user)
      parts.push part.id
      out << "<div id='partcontainer#{part.id}' class='excercise-part-container hidden'>"
      out << "<div id='part#{part.id}' class='abcTune' data-abc='#{part.notation.gsub("\r\n", "&#xa;")}' data-abcmodel='true'>image</div>".html_safe
      out << "<div class = 'play_container'>"
      out << "<a onclick=\"maestoso.abcObjects['part#{part.id}'].play(0, 1000)\"><div class='btn btn-info'>#{t('reading_excercise.play')}</div>&nbsp;&nbsp;</a>"
      out << "<a onclick=\"maestoso.part.readExcercise.rhythmPractice('part#{part.id}')\"><div class='btn btn-info'>#{t('reading_excercise.practice_rhythm')}</div></a>"
      out << "</div>"
      out << "</div>"
    end
    out << excercise_controls(exid, parts)
    out << "</div>"
    out

  end

  def reading_excercise_random(timesig, key, notes_str, steps, measure_parts)
    out = ''
    exid = Digest::MD5.hexdigest(measure_parts.join(",") + notes_str)
    #exid = Base64.encode64(measure_parts.join(","))
    out << "<div id= '#{exid}'>"
    notes = notes_str.split(" ")
    part_template = "X:1&#xa;M:#{timesig}&#xa;K:#{params[:key] || key}&#xa;L:1/32&#xa;Q:1/4=#{params[:tempo] || 60}&#xa;|"
    parts = []
    composer = Composer.new(timesig, key, notes, steps.split(' ').collect(&:to_i), measure_parts, )
    10.times do
      id = Time.now.to_f.to_s.gsub('.', '')
      parts.push id
      abc = composer.abc
      out << "<div id='partcontainer#{id}' class='excercise-part-container hidden'>"
      out << "<div id='part#{id}' class='abcTune' data-abc='#{part_template}#{abc.join}|'>image</div>".html_safe
      out << "<div class = 'play_container'>"
      out << "</div>"
      out << "</div>"
    end
    out << excercise_controls(exid, parts)
    out << "</div>"
    out
  end

  private

  def excercise_controls(exid, parts)
    out = ''
    out << "<a name='exid#{exid}'></a>"
    out << "<div class = excercise_controls>"
    out << t('reading_excercise.choose_excercise') + '&nbsp'
    out << select_tag('choose_excercise_' + exid, options_for_select([[t('reading_excercise.justread'), 'read'],[t('reading_excercise.practice_rhythm'), 'rhythm_practice'], [t('reading_excercise.finger_practice'), 'finger_practice']]), data: {exid: exid})
    out << '&nbsp &nbsp' + t('reading_excercise.choose_key') + '&nbsp'
    out << select_tag('choose_key_' + exid, options_for_select(Tune.keys, params[:key] || 'C'), class: 'choose_key choose_param', data: {exid: exid})
    out << '&nbsp &nbsp' + t('reading_excercise.choose_tempo') + '&nbsp'
    out << select_tag('choose_tempo_' + exid, options_for_select(%w(40 45 50 55 60 65 70 75 80 85 90 95 100 105 110 115 120), params[:tempo] || '60'), class: 'choose_tempo choose_param', data: {exid: exid})
    out << "<span class = 'start_container'>"
    out << link_to(t('reading_excercise.start'), "javascript:maestoso.part.readExcercise.rotate('#{exid}','#{parts.join(',')}')", class: "btn btn-small btn-transparent hide-on-play", data: {exid: exid} )
    out << '</span>'
    out << link_to(t('reading_excercise.next'), "javascript:;",  id: "play_next_#{exid}", class: "next-score btn btn-small btn-transparent hidden show-on-play",  data: {exid: exid} )
    #out << "<a onclick=\"maestoso.abcObjects['part' + maestoso.part.readExcercise.lastDisplayed].play(0, 1000)\" class='btn btn-small btn-transparent show-on-play hidden'>#{t('reading_excercise.play')}</a>"
    out << link_to(t('reading_excercise.repeat'), "javascript:;", class: "again-score btn btn-small btn-transparent hidden show-on-play",  data: {exid: exid} )
    out << link_to(t('reading_excercise.refresh'), book_chapter_url(@book, @chapter, time: Time.now.to_f.to_s,  anchor: "exid#{exid}"),  id: "refresh_#{exid}", class: "btn btn-small btn-transparent  hidden" )
    out << "</div>"
    out
  end


end