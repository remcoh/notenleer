module ApplicationHelper
  def navigation_menu
    if current_user.nil?
      role = "anonymous"
    else
      role = current_user.role
    end

    render "layouts/nav_#{role}" unless role == "anonymous"

  end

  def insert_tune_links(text)
    text.gsub(/\[(\d*)\]/) do |id|
      tune = Tune.find(id[1..-2].to_i)
      link_to tune.title, tune_url(tune, chapter_id: @chapter.id)
    end
  end

  def instruments
    Settings.instruments.collect { |i| [I18n.t("instrument.#{i}"), i] }.sort_by { |e| e[0] }
  end

end
