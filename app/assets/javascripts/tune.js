maestoso.tune = {
    index: function(){
        $(".tune_tags").select2({
            tags: true,
            placeholder: "Select tag",
            allowClear: true,
            minimumInputLength: 0,
            maximumSelectionSize: 1,
            width: '100%'
        })

        if (navigator.userAgent.match(/iPad/i) == null) {
            $("#filterrific_with_meter").select2();
            $("#filterrific_genre").select2();
            $("#filterrific_collection").select2();
            $("#list").select2();
        }
    }
}