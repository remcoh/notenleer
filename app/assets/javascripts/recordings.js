maestoso.recordings = {
    init: function() {
        $('#modal_recordings_ok').on('click', function () {
            $('.recordings .modal-body #recording_remarks').html("<h3>" + I18n.t('recording.uploading') +"</h3>")
            maestoso.RecordSession.sendWaveToPost(maestoso.recording)
        })

        if(maestoso.recordingIds.length > 0 && $('#selPractice').val() == "Record session"){
            $('#selBacking').parent().removeClass('hidden')
        }

        $('#recording_soundtrack').on('click', function(){
            $('#save_uploaded_file, #remarks').removeClass('hidden')
        })
        // TODO this somehow suddenly disables submit, investigate needed
        // $('#submit_upload').on('click', function(){
        //     $('#modal_upload .modal-body').html("<h3>" + I18n.t('recording.uploading') +"</h3>")
        // })

        $("#recording_study_track" ).on("change", function(){
            if($("#recording_study_track:checked" ).val() == "1"){
                $('#speed_versions_line').removeClass('hidden')
            }else{
                $('#speed_versions_line').addClass('hidden')
            }

        })
    }
}