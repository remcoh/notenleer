maestoso.comments = {
    init: function(){
        $('.post_comment').on('click', function(e){
            var text = $("#comment_text_" + e.currentTarget.dataset.commentableId ).val()
            if(text.length>0){
                $.post("/comments.js", {
                    commentable_id: e.currentTarget.dataset.commentableId,
                    commentable_type: e.currentTarget.dataset.commentableType,
                    text: $("#comment_text_" + e.currentTarget.dataset.commentableId ).val(),
                })
            }
            else{
                alert(I18n.t("comments.no_empty"))
            }


        })
    }

}