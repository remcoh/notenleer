maestoso.drawShape = {
    marknote: function(svgindex, x, y, height, color, opacity, nr, container) {
        var rect, svgns, textns, val;
        if(nr == undefined){
            nr = ''
        }
        svgns = "http://www.w3.org/2000/svg";
        rect = document.createElementNS(svgns, 'rect');
        rect.setAttributeNS(null, 'class', 'marker');
        rect.setAttributeNS(null, 'x', x - 10);
        rect.setAttributeNS(null, 'y', y);
        rect.setAttributeNS(null, 'height', height);
        rect.setAttributeNS(null, 'width', '30');
        rect.setAttributeNS(null, 'fill', color);
        rect.setAttributeNS(null, 'opacity', opacity);
        rect.setAttributeNS(null, 'data-nr', nr);
        rect.setAttributeNS(null, 'data-svgindex', svgindex);
        var text = document.createElementNS(svgns, "text");
        text.setAttributeNS(null, 'class', 'marker');
        text.setAttributeNS(null,"x",x);
        text.setAttributeNS(null,"y",y+10);
        text.setAttributeNS(null, 'opacity', 0.7);
        text.setAttributeNS(null,"font-size","8");
        var textNode = document.createTextNode(nr);
        text.appendChild(textNode);
        if(container == undefined){
            $('svg')[svgindex].appendChild(rect);
            $('svg')[svgindex].appendChild(text);
        }else{
            $('#' + container + ' svg')[svgindex].appendChild(rect);
            $('#' + container + ' svg')[svgindex].appendChild(text);
        }

    },

    circle: function(svgindex, x, y, r, color, borderColor, borderStroke,  opacity, container) {
        var rect, svgns;
        svgns = "http://www.w3.org/2000/svg";
        rect = document.createElementNS(svgns, 'circle');
        rect.setAttributeNS(null, 'cx', x);
        rect.setAttributeNS(null, 'cy', y);
        rect.setAttributeNS(null, 'r', r);
        rect.setAttributeNS(null, 'fill', color);
        rect.setAttributeNS(null, 'opacity', opacity);
        rect.setAttributeNS(null, 'stroke', borderColor);
        rect.setAttributeNS(null, 'stroke-width', borderStroke);
        if(container == undefined){
            $('svg')[svgindex].appendChild(rect);
        }else{
            $('#' + container + ' svg')[svgindex].appendChild(rect);
        }
    },

    line: function(svgindex, from, to, style, klass){
        var line, svgns;
        svgns = "http://www.w3.org/2000/svg";
        line = document.createElementNS(svgns, 'line');
        line.setAttributeNS(null, 'x1', from.x )
        line.setAttributeNS(null, 'y1', from.y )
        line.setAttributeNS(null, 'x2', to.x )
        line.setAttributeNS(null, 'y2', to.y )
        line.setAttributeNS(null, 'style', style)
        line.setAttributeNS(null, 'class', klass)
        $('svg')[svgindex].appendChild(line);

    }
};