Mixer = function(ids) {
    this.playing = false
    this.context = maestoso.audioContext,
    this.ids = ids
    this.sources = {}
    this.gainNodes = {}
    this.sliderVolumes = {}
    this.duration = 0
    this.slidersActive = false
    var self = this
    this.loadTracks = function () {
        for (i = 0; i < this.ids.length; i++) {
            audioFile = this._getAudioFromId(this.ids[i])
            console.log("audiofile", audioFile)
            WebAudiox.loadBuffer(this.context, audioFile, function (buffer) {})
            WebAudiox.loadBuffer.onLoad = function(context, audioFile, buffer){
                key = audioFile.split("/")[2]
                self.sources[key] = context.createBufferSource()
                self.sources[key].buffer = buffer
                volume = self.sliderVolumes[key]
                if(volume == undefined){volume = 0.5}
                self.gainNodes[key] = context.createGain();
                self.gainNodes[key].gain.value = volume
                console.log("gainnodes",self.gainNodes, key )
                maestoso.mixer.sources[key].connect(maestoso.mixer.gainNodes[key])
                lineOut = new WebAudiox.LineOut(context)
                maestoso.mixer.gainNodes[key].connect(lineOut.destination)

                maestoso.mixer.duration = Math.max(buffer.duration, maestoso.mixer.duration)
                
                // if all tracks are loaded
                if(Object.keys(maestoso.mixer.sources).length == maestoso.mixer.ids.length && !maestoso.mixer.slidersActive){
                    maestoso.mixer.slidersActive = true
                    maestoso.mixer.initSliders()
                }
            }

        }
    }

    this.initSliders = function(){
        for(i=0;i<$(".volume_slider").length; i++){
            $("#vol" + $(".volume_slider")[i].dataset.sliderId).slider().on('slideStop',
                function (v) {
                    maestoso.mixer.sliderVolumes[v.currentTarget.dataset.trackFile] = v.value
                    maestoso.mixer.gainNodes[v.currentTarget.dataset.trackFile].gain.value = v.value

                }
            ).data('slider');
        }
        $('#position_mix').slider({max: maestoso.mixer.duration}).on('slideStop', function(v){
            console.log("sliding", v)
            maestoso.mixer.stopTracks()
            maestoso.mixer.loadAndPlay(v.value)
        })
    }

    this.playTracks = function (start) {
        console.log("playtracks called")
        //if(this.ids.length == 0)return
        if(!this.playing) {
            this.playing = true
            if (start == undefined) {
                start = 0
            }
            for (i = 0; i < this.ids.length; i++) {
                this.sources[this.ids[i]].start(0, start)
            }

            maestoso.mixer.positionUpdater = window.setInterval(function () {
                $("#position_mix").slider('setValue', parseInt($("#position_mix").val()) + 1)

            }, 1000)

        }
    }


    this.stopTracks = function () {
        if(this.playing) {
            this.playing = false
            for (i = 0; i < this.ids.length; i++) {
                this.sources[this.ids[i]].stop()
            }
            clearInterval(maestoso.mixer.positionUpdater)
        }
    }

    this.loadAndPlay = function(start){
        if(!this.playing) {
            this.loadTracks()
            self = this
            window.setTimeout(
                function () {
                    self.playTracks(start)
                }, 2000)
        }

    }

    this._getAudioFromId = function(){
        var f = "/audios/" + this.ids[i]
        return(f)
    }
}


