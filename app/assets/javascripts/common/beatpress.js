maestoso.beatPress = {
    registerEvent: function(){
        var that = this
        console.log("registering event")
        $(window).unbind("keypress");
        $(window).keypress(function(e) {
            if ((e.keyCode === 0 || e.keyCode === 32)) {
                maestoso.beatPress.sendBeatPress()
                e.preventDefault();
            }
        });
        document.body.addEventListener('touchstart', function() { maestoso.beatPress.sendBeatPress() })
    },

    unregisterEvent: function(){
        $(window).unbind("keypress");
    },
    
    sendBeatPress: function(){
        var beatPress = new CustomEvent("beatPressed", {});
        document.dispatchEvent(beatPress)
    }

}






