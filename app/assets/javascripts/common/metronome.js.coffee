# This metronome can be used like:
# m=new maestoso.Metronome(60, playHihat, 4, function(){player.startPlay( myEditor.tune[0].midi )})
# We calculate the bmp interval from the Math.floor from the ticktime so the beat will be in sync with the player
# 9-11-2016 Ik snap niet meer precies hoe het zit met de ticktime, en waarom het zo gedaan is, ik bewaar code code juist in case
# maar nu weer conventionele berekening om het in sync te laten lopen met ritme oefening
window.maestoso.Metronome = class Metronome
  constructor: (@bpm, @playSound, @beatsToCount, @callback, @stopAfterBeats) ->
    @count = 0

  start: ->
    teller = 0
    @timer = window.setInterval =>
       #console.log("Metronome time", Date.now())
       @playSound()
       @count += 1
       @callback() if @count == @beatsToCount
       clearInterval @timer if @count == @beatsToCount && @stopAfterBeats
    , (60 / @bpm) * 1000 #8 * Math.floor(@_tickTime(@bpm))
    return

  stop: ->
    clearInterval @timer
    return

  _tickTime: (bpm) ->
    (60000/bpm)/8




