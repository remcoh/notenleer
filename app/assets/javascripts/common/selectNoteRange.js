maestoso.animationRange = {
    from: 0,
    to: 0,
    svgindex: 0,
    firstMarker: true,
    showMarkers: function(fromOrTo, maxTo, svgindex) {
        console.log("show markers called", fromOrTo, maxTo, svgindex)
        var i, lastxpos, len, multiSvg, note, nr, ref;
        multiSvg = $('svg').length > 1;
        if (fromOrTo != undefined) {
            if (this.firstMarker) {
                this.from = fromOrTo;
                this.to = maxTo;
                this.start_svgindex = svgindex;
            } else {
                this.to = fromOrTo;
            }
            this.svgindex = this.start_svgindex;
        } else {
            this.from = 0;
            this.to = maxTo;
        }
        if (fromOrTo != undefined) {
            this.firstMarker = !this.firstMarker;
        }
        nr = 0;
        $('.marker').remove();
        nr = this.from;
        ref = maestoso.currentTune.notesTohit.slice(this.from, +this.to + 1 || 9e9);
        for (i = 0, len = ref.length; i < len; i++) {
            note = ref[i];
            if (lastxpos > 0 && note.xpos < lastxpos && multiSvg) {
                this.svgindex = this.svgindex + 1;
            }

            maestoso.drawShape.marknote(this.svgindex, maestoso.part.cfactor * note.xpos, maestoso.part.cfactor * note.ypos - 20, 35, "blue", 0.5, nr);
            lastxpos = note.xpos;
            nr += 1;
        }
        return $('.marker').on('click', function(e) {
            return maestoso.animationRange.showMarkers($(this).data().nr, maxTo, $(this).data().svgindex);
        });
    }
};
