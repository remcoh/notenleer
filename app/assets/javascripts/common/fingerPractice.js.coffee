window.maestoso.fingerPractice = class FingerPractice
  constructor: (sampleTime, container) ->
    @sampleTime = 20
    @container = container
  start: ->
    clearInterval @timer
    d = new Date
    [@good, @bad] = [0, 0, 0]
    @lastNote = window.maestoso.currentTune.notesTohit[0]
    @y = @lastNote.ypos
    @sampleStartTime = d.getTime()
    @timer = window.setInterval =>
      d = new Date
      sampleTime = d.getTime() - @sampleStartTime
      notes = window.maestoso.currentTune.notesTohit
      [prevNote, shouldNote, nextNote] = @findNotesForTime(window.maestoso.currentTune.notesTohit, sampleTime)
      maestoso.drawShape.circle(shouldNote.svgIndex, shouldNote.xpos, @y, 5, 'white', 'grey', 1, 0.5, @container)
      result = @result_keys(shouldNote, prevNote, nextNote, sampleTime)
      unless shouldNote == undefined
        if shouldNote.xpos != @lastNote.xpos
          @markNote(@good, @bad, @lastNote.xpos, @y, @lastNote.svgIndex) if @good > 0 or @bad > 0
          if shouldNote.xpos < @lastNote.xpos
            @y = shouldNote.ypos
          @lastNote = shouldNote
        if result then @good = @good + 1 else @bad = @bad + 1
    , @sampleTime

  stop: ->
    $('circle').remove()
    clearInterval @timer
    maestoso.metronome.stop()
    return

  markNote: (good, bad, x, y, svgIndex)=>
    color = if good / bad > 1 then "green" else "red"
    maestoso.drawShape.circle(svgIndex, x, y , 5, color,color, 2, 0.5, @container)
    [@good, @bad] = [0, 0]
    return

  result_keys: (shouldNote, prevNote, nextNote, time) =>
    return undefined if shouldNote == undefined
    return true if shouldNote.pitch == 0
    return true if @keysFromPitch(shouldNote.pitch).join() == maestoso.pressed_keys.join()
    return true if prevNote && @keysFromPitch(prevNote.pitch).join() == maestoso.pressed_keys.join() && time > shouldNote.startTime && time < shouldNote.startTime + 100
    return true if nextNote && @keysFromPitch(nextNote.pitch).join() == maestoso.pressed_keys.join() && time > shouldNote.endTime && time < shouldNote.endTime + 100

  findNotesForTime: (notes, time) =>
    shouldNoteArr = notes.filter (e)->
      return time > e.startTime && time < e.endTime
    shouldNote = shouldNoteArr[0]
    prevNote = notes[notes.indexOf(shouldNote) - 1]
    nextNote = notes[notes.indexOf(shouldNote) + 1]
    [prevNote, shouldNote, nextNote]


  keysFromPitch: (pitch) =>
    keysMap =
      54: [1,2,3]
      55: [1,3]
      56: [2,3]
      57: [1,2]
      58: [1]
      59: [2]
      60: []
      61: [1,2,3]
      62: [1,3]
      63: [2,3]
      64: [1,2]
      65: [1]
      66: [2]
      67: []
      68: [2,3]
      69: [1,2]
      70: [1]
      71: [2]
      72: []  #c
      73: [1,2]
      74: [1]
      75: [2]
      76: []
      77: [1]
      78: [2]
      79: []
      80: [2,3]
      81: [1,2] #a
      82: [1]
      83: [2]
      84: []
    keys =  keysMap[pitch]
    if keys == undefined then return [] else return keys
