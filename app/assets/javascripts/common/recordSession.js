window.maestoso.RecordSession = {
    startUserMedia: function(stream) {
        var input;
        input = maestoso.audioContext.createMediaStreamSource(stream);
        window.maestoso.recorder = new Recorder(input);
    },
    startRecording: function(button) {
        this.started = true;
        maestoso.recorder && maestoso.recorder.record();
    },
    stopRecording: function(button) {
        if (this.started) {
            if (maestoso.playingAudio !== void 0) {
                maestoso.playingAudio.pause();
            }
            maestoso.recorder && maestoso.recorder.stop();
            maestoso.RecordSession.createDownloadLink();
            maestoso.recorder.clear();
            this.started = false;
        }
    },
    createDownloadLink: function() {
        console.log('create download', maestoso.recorder);
        maestoso.recorder && maestoso.recorder.exportWAV(function(blob) {
            var au, li, url;
            url = URL.createObjectURL(blob);
            li = document.createElement('li');
            au = document.createElement('audio');
            au.controls = true;
            au.src = url;
            li.appendChild(au);
            while (recordingslist.firstChild) {
                recordingslist.removeChild(recordingslist.firstChild);
            }
            recordingslist.appendChild(li);
        });
    },
    sendWaveToPost: function(blob) {
        var data, oReq;
        data = new FormData;
        data.append('audio', blob, (new Date).getTime() + '.wav');
        data.append('authenticity_token', AUTH_TOKEN);
        data.append('part_id', maestoso.part_id);
        data.append('user_id', $('#user_id').val());
        data.append('remarks', $('#remarks').val());
        data.append('access', $('#access').val());
        oReq = new XMLHttpRequest;
        oReq.open('POST', '/recordings.js');
        oReq.send(data);
        oReq.onload = function(oEvent) {
            if (oReq.status === 200) {
                console.log(oReq);
                console.log('Uploaded');
                eval(oReq.responseText);
                $('#modal_recordings').modal('hide')
            } else {
                console.log('Error ' + oReq.status + ' occurred uploading your file.');
                $('#modal_recordings').modal('hide')
            }
        };
    }
};
