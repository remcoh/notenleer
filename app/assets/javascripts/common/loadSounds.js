var hihatBuffer = null;
// Fix up prefixing
window.AudioContext = window.AudioContext || window.webkitAudioContext;
var context = new AudioContext();

function loadhihatBuffer() {
  var request = new XMLHttpRequest();
  request.open('GET', "/hihat.wav", true);
  request.responseType = 'arraybuffer';

  // Decode asynchronously
  request.onload = function() {
    context.decodeAudioData(request.response, function(buffer) {
      hihatBuffer = buffer;
    }, onError);
  }
  request.send();
}

function onError(){
  console.log("error")
}

function frequencyFromNoteNumber( note ) {
	return 440 * Math.pow(2,(note-69)/12);
}

maestoso.sounds = {
  init: function(){
    maestoso.sounds.hihatsynth = new Tone.MetalSynth({
      frequency : 600 ,
      envelope : {
        attack : 0.005 ,
        decay : 0.02,
        sustain : 0
      },
      harmonicity : 5.1 ,
      modulationIndex : 32 ,
      resonance : 4000 ,
      octaves : 1.5
    }).toMaster();

    maestoso.sounds.pianosynth = new Tone.Synth({oscillator: {type:'sawtooth3'}}).toMaster()

    Soundfont.instrument(new AudioContext(), 'acoustic_grand_piano').then(function (piano) {
      maestoso.sounds.pianosf = piano
    })
  },

  hiHat: function(){
    maestoso.sounds.hihatsynth.triggerAttackRelease('16n')
  },

  piano: function(n,d){
    maestoso.sounds.pianosf.play(n)
    //maestoso.sounds.pianosynth.triggerAttackRelease(n, d)
  },

  synth: function(note){
      var attack = 20,
          decay = 250,
          gain = context.createGain(),
          osc = context.createOscillator();

      gain.connect(context.destination);
      gain.gain.setValueAtTime(0, context.currentTime);
      gain.gain.linearRampToValueAtTime(1, context.currentTime + attack / 1000);
      gain.gain.linearRampToValueAtTime(0, context.currentTime + decay / 1000);

      osc.frequency.value = frequencyFromNoteNumber(note);
      osc.type = "triangle";
      osc.connect(gain);
      osc.start(0);

      setTimeout(function() {
          osc.stop(0);
          osc.disconnect(gain);
          gain.disconnect(context.destination);
      }, decay)

  },

  soundFont: function(note){
      maestoso.instrument.play(note, 0);
  }

}
