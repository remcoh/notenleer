window.maestoso.livePlayPractice = class livePlayPractice
  constructor: (sampleTime) ->
    @sampleTime = 50
    @xOffset = 0
    @lastNote = window.maestoso.currentTune.notesTohit[0]

  start: ->
    clearInterval @timer
    d = new Date
    @sampleStartTime = d.getTime()
    @_drawAxisses()
    @timer = window.setInterval =>
      d = new Date
      sampleTime = d.getTime() - @sampleStartTime
      notes = window.maestoso.currentTune.notesTohit
      [prevNote, shouldNote, nextNote] = @findNotesForTime(window.maestoso.currentTune.notesTohit, sampleTime)
      if shouldNote == undefined
        @stop
      else
        if shouldNote.xpos != @lastNote.xpos # if new note
          @xOffset = 0
          @lastNote = shouldNote
        @xOffset = @_calcXoffset shouldNote, nextNote
        @_plot shouldNote
    , @sampleTime

  stop: ->
    $('circle').remove()
    $('line').remove()
    clearInterval @timer
    maestoso.metronome.stop()
    @xOffset = 0
    @lastNote = window.maestoso.currentTune.notesTohit[0]
    return

  findNotesForTime: (notes, time) =>
    shouldNoteArr = notes.filter (e)->
      return time > e.startTime && time < e.endTime
    shouldNote = shouldNoteArr[0]
    prevNote = notes[notes.indexOf(shouldNote) - 1]
    nextNote = notes[notes.indexOf(shouldNote) + 1]
    [prevNote, shouldNote, nextNote]

  _drawAxisses: ->
    svgs = $('svg')
    @baseAxisses = []
    for svg in svgs
      @baseAxisses.push (svg.getBoundingClientRect().bottom - svg.getBoundingClientRect().top) - 50
    i = 0
    @baseAxisses[0] = @baseAxisses[0] + 30
    while i < @baseAxisses.length
      maestoso.drawShape.line(i,  { x: 0, y: @baseAxisses[i]/2 }, { x: 1100, y:  @baseAxisses[i]/2 }, 'stroke:#666666;stroke-width:1')
      i++


  _calcXincrement: (shouldNote, nextNote) ->
    distance = nextNote.xpos - shouldNote.xpos
    time =  nextNote.startTime - shouldNote.startTime
    distance / (time / @sampleTime)

  _calcXoffset: (shouldNote, nextNote) ->
    if nextNote.xpos > shouldNote.xpos
      @xOffset = @xOffset + @_calcXincrement shouldNote, nextNote
    else
      @xOffset = @xOffset + 1

  _cents: (shouldNote) ->
    ratio = shouldNote.freq / maestoso.currentNote.pitch
    cents = 1200 * Math.log2(ratio)
    #console.log("cents", shouldNote.freq, maestoso.currentNote.pitch, ratio, cents)
    cents

  _plot: (shouldNote) ->
    xPos = shouldNote.xpos + parseInt(@xOffset)
    baseYpos = @baseAxisses[shouldNote.svgIndex]/2
    $('.cursor').remove()
    maestoso.drawShape.line(shouldNote.svgIndex, { x: xPos, y: baseYpos - 50 }, { x: xPos, y: baseYpos + 50 }, 'stroke:blue;stroke-width:2', 'cursor' )
    if maestoso.currentNote.pitch > 0
      cents = @_cents(shouldNote)
      console.log("cents", cents)
      if Math.abs(cents) < 50
        maestoso.drawShape.circle(shouldNote.svgIndex, xPos, baseYpos + cents , 2, 'green', 'green', 1)
      else
        maestoso.drawShape.circle(shouldNote.svgIndex, xPos, baseYpos , 2, 'red', 'red', 1)
    else
      maestoso.drawShape.circle(shouldNote.svgIndex, xPos, @baseAxisses[shouldNote.svgIndex]/2 , 2, 'gray')






