maestoso.pressed_keys = [];
var keysMap = {188: 1, 190: 2, 191: 3}
window.addEventListener("keydown",
    function(e){
        brassKey = keysMap[e.keyCode]
        if(maestoso.pressed_keys.indexOf(brassKey) == -1){
            maestoso.pressed_keys.push(brassKey);
            maestoso.pressed_keys = maestoso.pressed_keys.sort()
        }
    });

window.addEventListener('keyup',
    function(e){
        brassKey = keysMap[e.keyCode]
        var i = maestoso.pressed_keys.indexOf(brassKey);
        if(i != -1) {
            maestoso.pressed_keys.splice(i, 1);
        }


    });

