maestoso.inlineAbcRenderer = {
    init: function(){
        maestoso.abcObjects.hittimes = {}
        maestoso.positions = {}
        var image_buffer = {}
        var tunes = $(".abcTune")
        for (t = 0; t < tunes.length; t++) {
            maestoso.abcObjects[tunes[t].getAttribute("id")] = new AbcPlay
            maestoso.abcObjects.hittimes[tunes[t].getAttribute("id")] = []
            maestoso.positions[tunes[t].getAttribute("id")] = {}
            maestoso.inlineAbcRenderer.to_svg(tunes[t], image_buffer)
            var svgIndex = 0
            var lastxPos = 0
            for(i=0; i<maestoso.abcObjects.hittimes[tunes[t].getAttribute("id")].length; i++) {
                var tuneId = tunes[t].getAttribute("id")
                maestoso.abcObjects.hittimes[tuneId][i]["xpos"] = maestoso.positions[tuneId][maestoso.abcObjects.hittimes[tuneId][i].index].x;
                maestoso.abcObjects.hittimes[tuneId][i]["ypos"] = maestoso.positions[tuneId][maestoso.abcObjects.hittimes[tuneId][i].index].y;
                if(maestoso.abcObjects.hittimes[tuneId]["xpos"] < lastxPos && lastxPos > 0){
                    svgIndex = svgIndex + 1
                }
                maestoso.abcObjects.hittimes[tuneId][i]["svgIndex"] = svgIndex
                lastxPos = maestoso.abcObjects.hittimes[tuneId][i]["xpos"]
            }
        }

        window.setTimeout(function () {
            for (t = 0; t < tunes.length; t++) {
                tunes[t].innerHTML = image_buffer[tunes[t].getAttribute("id")].svg;
            }
        }, 2000)
    },

    to_svg: function(abc, image_buffer){
        image_buffer[abc.getAttribute("id")] = {svg: ''}
        var wfactor = abc.dataset.wfactor || 1
        cb = {
            img_out: function (i) {
                image_buffer[abc.getAttribute("id")].svg += i
            },
            errbld: function (i, j, k) {
                console.log("errbld called", i, j, k)
            },
            read_file: function (i) {
                console.log("read_file called", i)
            },

            get_abcmodel: function (tsfirst, voice_tb, music_types, info) {
                maestoso.abcplay.add(tsfirst, voice_tb[0].key)
            },
            anno_stop: function (type, start, stop, x, y, w, h) {
              window.maestoso.positions[abc.getAttribute("id")][start] = {x: x + 70, y: y + 90}
            },

            imagesize: 'width="' + ($('.main').width() * wfactor )+  '"'
        }
        cb.get_abcmodel = function (tsfirst, voice_tb, music_types, info) {
            window.maestoso.abcObjects[abc.getAttribute("id")].add(tsfirst, voice_tb[0].key, abc.getAttribute("id") )
        }
        var r = new Abc(cb)
        r.tosvg("file_name", abc.dataset.abc)

    }

}
