maestoso.countdown = {
    countdownPlayerStart: function (bpm, doAfterCount) {
        $("#countDown").removeClass("hidden")
        maestoso.metronome = new maestoso.Metronome(bpm,
            function () {
                countdown = maestoso.metronome.beatsToCount - maestoso.metronome.count
                maestoso.sounds.hiHat()
                if (countdown > 1) {

                    $("#countDown").html(maestoso.metronome.beatsToCount - maestoso.metronome.count - 1)
                }
                else {
                    $("#countDown").addClass("hidden")
                }
            },
            5,
            doAfterCount)
        maestoso.metronome.start()
    }
}
