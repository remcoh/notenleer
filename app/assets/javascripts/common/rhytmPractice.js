var marknote;

maestoso.rhytmPractice = {
    started: false,
    mode: "practice",
    syncTune: null,
    notes: [],
    notesToHit: [],
    hits: [],
    accuracy: 100,
    missedNotes: 0,
    goodNotes: 0,
    failedAttempts: 0,
    successfullAttempts: 0,
    filterBlocked: false,
    hitNotes: [],
    timeouts: [],
    playerStarted: false,
    playAlong: false,
    withMetronome: true,
    animateFrom: null,
    animateTo: null,
    firstMarker: true,
    beatPressRegistered: false,
    bpm: 60,
    onStop: null,
    start: function(speedFactor, onStop) {
        

        this.onStop = onStop
        var e;
        this.speedFactor = speedFactor;
        console.log("speedfactor", speedFactor)
        that = this
        if(this.beatPressRegistered == false){
            document.addEventListener("beatPressed", function(){maestoso.rhytmPractice.registerBeat()})
            this.beatPressRegistered = true
        }
        this.started = true;
        $('.marker').remove();
        this.hits = [];
        this.notesToHitArr = Object.create(window.maestoso.currentTune.notesTohit.slice(0, +maestoso.currentTune.notesTohit.length + 1 || 9e9));
        this.started = true;
        try {
            player.stopPlay();
        } catch (_error) {
            e = _error;
        }
        try {
            playButton.removeClass("gl_disabled");
        } catch (_error) {
            e = _error;
        }
        try {
            stopButton.addClass("gl_disabled");
        } catch (_error) {
            e = _error;
        }
        this.metronome = new maestoso.Metronome(this.bpm, maestoso.sounds.hiHat);
        console.log("starting metronome", this.withMetronome)
        if (this.withMetronome) {
            console.log("starting metronome")
            this.metronome.start();
        }
    },
    stop: function(result) {
        var e, i, len, ref, t;
        if (this.started) {
            this.started = false;
            this.songStartTime = void 0;
            if (this.metronome !== void 0) {
                this.metronome.stop();
            }
            try {
                player.stopPlay();
            } catch (_error) {
                e = _error;
            }
            if (maestoso.playingAudio !== void 0) {
                maestoso.playingAudio.pause();
            }
            this.missedNotes = 0;
            this.goodNotes = 0;
            ref = this.timeouts;
            for (i = 0, len = ref.length; i < len; i++) {
                t = ref[i];
                window.clearTimeout(t);
            }
            this.onStop()
        }
        maestoso.beatPress.unregisterEvent()
    },
    registerBeat: function() {
        var absDev, clocktime, color, d, dev, green, hitTime, lDev, noteToHit, red, result;
        if (!this.started) {
            return;
        }
        if (!(this.playerStarted || !this.playAlong)) {
            player.startPlay(myEditor.tunes[0].midi);
            this.playerStarted = true;
        }
        d = new Date;
        clocktime = d.getTime();
        if (this.songStartTime === void 0 && this.started) {
            this.songStartTime = clocktime;
        }
        hitTime = (clocktime - this.songStartTime) * this.speedFactor;
        noteToHit = this.notesToHitArr.shift();
        if (this.mode === 'sync') {
            this.hits.push(hitTime);
        }
        result = hitTime > noteToHit.startTime - this.accuracy && hitTime < noteToHit.startTime + this.accuracy;
        noteToHit.hitTime = hitTime;
        this.hitNotes.push(noteToHit);
        dev = Math.round(255 / 150 * (hitTime - noteToHit.startTime));
        if (dev > 0) {
            lDev = Math.min(dev, 255);
        } else {
            lDev = Math.max(dev, -255);
        }
        absDev = Math.abs(lDev);
        red = ('0' + absDev.toString(16)).slice(-2);
        green = ('0' + (255 - absDev).toString(16)).slice(-2);
        color = ('#' + red + green + '00').toUpperCase();
        maestoso.sounds.synth(noteToHit.pitch);
        maestoso.drawShape.marknote(noteToHit.svgIndex, noteToHit.xpos,  noteToHit.ypos +18 + lDev / 20, 5, color, 0.5, undefined, maestoso.rhytmPractice.container);
        maestoso.part.autoscroll.scrollIfNeeded(noteToHit.ypos);
        if (absDev >= 255) {
            this.missedNotes += 1;
            if (this.missedNotes >= 3) {
                $( ".failure .modal-header").html('<h3>' + I18n.t('part.rhytmpractice.failure_modal_header') + '</h3>')
                $( ".failure .modal-body").html(I18n.t('part.rhytmpractice.failure_modal_body') + this.goodNotes)
                $("#modal_failed").modal();
                this.stop('failed');

            }
        }else{
            this.goodNotes += 1;
        }
        if (this.notesToHitArr.length === 0) {
            this.stop('succeeded');
            $("#modal_succeeded").modal();
        }
    },

    _lastMark: function() {
        if (this.mode === "sync") {
            return maestoso.currentTune.notesTohit.length;
        } else {
            return window.maestoso.hittimes.length - 1;
        }
    }
};
