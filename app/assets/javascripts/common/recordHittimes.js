maestoso.recordHittimes = {
    started: false,
    syncTune: null,
    notes: [],
    notesToHit: [],
    hits: [],
    hitNotes: [],
    timeouts: [],
    playerStarted: false,
    init: function(speedfactor){
        this.speedFactor = speedfactor;
        maestoso.playingAudio = new Audio("/audios/" + this._audioFile(maestoso.study_track));
    },
    start: function() {
        var e, that;
        this.started = true;
        document.addEventListener("beatPressed", function(){that.registerBeat()})
        $('.marker').remove();
        this.hits = [];
        this.notesToHitArr = Object.create(window.maestoso.currentTune.notesTohit.slice(maestoso.animationRange.from, +maestoso.animationRange.to + 1 || 9e9));
        window.scrollTo(0, 0);
        try {
            player.stopPlay();
        } catch (_error) {
            e = _error;
        }
        try {
            playButton.removeClass("gl_disabled");
        } catch (_error) {
            e = _error;
        }
        try {
            stopButton.addClass("gl_disabled");
        } catch (_error) {
            e = _error;
        }
        that = this;
        this.songStartTime = Date.now();
        maestoso.playingAudio.play();
        this.hits.push("/audios/" + this.syncTune);
    },
    _audioReady: function(audio) {
        return $.when.apply($, audio.map(function() {
            var ready;
            ready = new $.Deferred;
            $(this).one('canplay', ready.resolve);
            return ready.promise();
        }));
    },
    stop: function(result) {
        var e, i, len, ref, t;
        if (this.started) {
            this.started = false;
            this.songStartTime = void 0;
            try {
                player.stopPlay();
            } catch (_error) {
                e = _error;
            }
            if (maestoso.playingAudio !== void 0) {
                maestoso.playingAudio.pause();
            }
            ref = this.timeouts;
            for (i = 0, len = ref.length; i < len; i++) {
                t = ref[i];
                window.clearTimeout(t);
            }
            $('#selStart').parent().removeClass('hidden');
            $('#selStop,#selPauze,#selContinue').parent().addClass('hidden');
            $(".canvas_wrapper").animate({
                "margin-top": "0px"
            });
        }
    },
    registerBeat: function() {
        var clocktime, d, hitTime, noteToHit, result;
        if (!this.started) {
            return;
        }
        d = new Date;
        clocktime = d.getTime();
        if (this.songStartTime === void 0 && this.started) {
            this.songStartTime = clocktime;
        }
        hitTime = (clocktime - this.songStartTime) * this.speedFactor;
        noteToHit = this.notesToHitArr.shift();
        this.hits.push(hitTime);
        result = hitTime > noteToHit.startTime - this.accuracy && hitTime < noteToHit.startTime + this.accuracy;
        noteToHit.hitTime = hitTime;
        this.hitNotes.push(noteToHit);
        var correctedX = ($('.score_container').width()/1140) * noteToHit.xpos
        maestoso.drawShape.marknote(noteToHit.svgIndex, correctedX, noteToHit.ypos, 5, "blue", 0.5);
        maestoso.part.autoscroll.scrollIfNeeded(noteToHit.svgIndex, noteToHit.ypos);
        if (this.notesToHitArr.length === 0) {
            this.updateHittimes();
            this.stop();
        }
    },
    updateHittimes: function() {
        var firstPart, hits, secondPart, thirdPart;
        if (maestoso.animationRange.from > 0) {
            firstPart = maestoso.hittimes.slice(0, maestoso.animationRange.from + 1);
            this.hits.shift();
            secondPart = this.hits;
            thirdPart = maestoso.hittimes.slice(maestoso.animationRange.to + 2, maestoso.hittimes.length - 1);
            hits = firstPart.concat(secondPart).concat(thirdPart);
        } else {
            hits = maestoso.recordHittimes.hits;
        }
        return $.ajax({
            url: '/parts/update_hittimes/' + window.location.href.split('/').last(),
            method: 'put',
            data: {
                hit_times: hits.join(',')
            }
        });
    },
    _audioFile: function(af) {
        if (this.speedFactor === 1) {
            return af;
        }
        return af.replace(/\.[^.$]+$/, '') + ("_" + (this.speedFactor * 100) + ".mp3");
    }
};