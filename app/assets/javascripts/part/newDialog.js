maestoso.part.new = function(){
        $("[name='instrument']" ).on("change", function(){
            instrument = $('#instrument').val()
        })
        $('#modal_new_part .modal_success_close').on('click', function () {
            qs = 'instrument=' + instrument
                               + "&key=" + $("#key").val()
                               + "&meter=" + $("#meter").val()
                               + "&title=" + $("#title").val()
                               + "&group_id=" + $("#group_id").val()
                               + "&part_type=" + $("#part_type").val()
                               + "&part_number=" + $("#part_number").val()
                               + "&multipart=" + $("#multipart").val()
            if(maestoso.tune_id == undefined){  // we are not working with a tune allready, so make a new one
                console.log("create new tune")
                window.location.href = '/tunes/new' + '?' + qs

            }else{
                window.location.href = '/parts/new?tune_id=' + maestoso.tune_id + '&' + qs
            }
        })
    }
    
