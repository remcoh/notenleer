maestoso.part.noteMarker = {
    init: function(){
        $("#action_add_note_marks").on("click", function(){
            $("#action_add_note_marks").addClass("hidden")
            $("#action_save_note_marks").removeClass("hidden")
            maestoso.noteMarks = []
        })

        $("#action_save_note_marks").on("click", function(){
            $("#action_save_note_marks").addClass("hidden")
            $("#action_add_note_marks").removeClass("hidden")
            $.post("/parts/update_note_marks/" + maestoso.part_id, {
                note_marks: JSON.stringify(maestoso.noteMarks)
            })
            maestoso.currentTune.notesTohit = maestoso.noteMarks
        })

        $("svg, .pdf_overlay").on("click", function(e){
            if($("#action_add_note_marks").hasClass("hidden")){
                console.log("mark", e.clientY- $("svg, .pdf_overlay").offset().top + $(document).scrollTop(), e.clientX - $("svg, .pdf_overlay").offset().left)

                x = e.clientX - $("svg, .pdf_overlay").offset().left
                var referenceX = parseInt((1140/$('.score_container').width()) * x)
                y = e.clientY- $("svg, .pdf_overlay").offset().top + $(document).scrollTop()

                maestoso.drawShape.circle( 0, x, y, 5, "blue", 0.5)
                maestoso.noteMarks.push({xpos: referenceX, ypos: y, svgIndex: 0 })

            }
        })
    }
}
