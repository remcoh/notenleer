maestoso.part.playAnimations = {
    init: function(speedFactor){
        this.speedFactor = speedFactor;
        maestoso.playingAudio = new Audio(this._audioFile(maestoso.hittimes[0]));
    },
    play: function() {
        var that;
        this.started = true;
        $('.marker').remove();
        this.notestoAnimate = [];
        maestoso.part.autoscroll.currentScroll = 0
        if (maestoso.animationRange.from > 0) {
            this.offset = parseInt(maestoso.hittimes[maestoso.animationRange.from + 1]);
        } else {
            this.offset = 0;
        }
        that = this;
        maestoso.playingAudio.currentTime = (this.offset * 1 / this.speedFactor) / 1000;
        var hit, i, j, k, len, len1, note, ref, ref1;
        maestoso.playingAudio.play();
        ref = maestoso.currentTune.notesTohit.slice(maestoso.animationRange.from, +maestoso.animationRange.to + 1 || 9e9);
        for (j = 0, len = ref.length; j < len; j++) {
            note = ref[j];
            that.notestoAnimate.push(note);
        }
        that.timeouts = [];
        ref1 = maestoso.hittimes.slice(maestoso.animationRange.from, +maestoso.animationRange.to + 1 || 9e9).slice(1);
        for (i = k = 0, len1 = ref1.length; k < len1; i = ++k) {
            hit = ref1[i];
            that.timeouts.push(window.setTimeout(function() {
                return that.animate();
            }, (hit - that.offset) * 1 / that.speedFactor));
        }
        that.timeouts.push(window.setTimeout(function() {
            return maestoso.playingAudio.pause();
        }, (maestoso.hittimes[maestoso.animationRange.to] - that.offset) * 1 / that.speedFactor));
        return that.timeouts.push(window.setTimeout(function() {
            maestoso.part.autoscroll.reset()
            return that.play(that.speedFactor);
        }, (maestoso.hittimes[maestoso.animationRange.to] - that.offset) * 1 / that.speedFactor + 2000));

    },
    animate: function() {
        var noteToMark;
        noteToMark = this.notestoAnimate.shift();
        $('.marker').remove();
        maestoso.part.autoscroll.scrollIfNeeded(noteToMark.svgIndex, noteToMark.ypos);
        return maestoso.drawShape.marknote(noteToMark.svgIndex, maestoso.part.cfactor * noteToMark.xpos, maestoso.part.cfactor * noteToMark.ypos - 20, 50, "green", 0.2);
    },
    stop: function() {
        var j, len, ref, results, t;
        if (maestoso.playingAudio !== void 0) {
            maestoso.playingAudio.pause();
        }
        ref = this.timeouts;
        results = [];
        for (j = 0, len = ref.length; j < len; j++) {
            t = ref[j];
            results.push(window.clearTimeout(t));
        }
        return results;
    },

    _audioFile: function(af) {
        if (this.speedFactor === 1) {
            return af;
        }
        return af.replace(/\.[^.$]+$/, '') + ("_" + (this.speedFactor * 100) + ".mp3");
    }
};
