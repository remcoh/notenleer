maestoso.selectBackingTracks = {
    init: function () {
        track_checkboxes = $(".backing_track")
        maestoso.part_tracks = []
        for (i = 0; i < track_checkboxes.length; i++) {
            maestoso.part_tracks.push(track_checkboxes[i].value)
        }

        $('.backing_track, #checkAll').on('click', function () {
            maestoso.mixer.stopTracks()
            track_checkboxes = $(".backing_track")
            maestoso.backing_tracks = []
            maestoso.mixer.ids = []
            $('#modal_backing_stop').addClass("hidden")
            $('#modal_backing_play').removeClass("hidden")

            for (i = 0; i < track_checkboxes.length; i++) {
                if (track_checkboxes[i].checked) {
                    maestoso.backing_tracks.push(track_checkboxes[i].value)
                    maestoso.mixer.ids.push(track_checkboxes[i].value)
                }
            }
        })

        $('#modal_backing_play').on('click', function () {
            $('#modal_backing_stop').removeClass("hidden")
            $('#modal_backing_play').addClass("hidden")
            maestoso.mixer.loadAndPlay(parseInt($('#position_mix').val()))
        })

        $('#modal_backing_stop').on('click', function () {
            $('#modal_backing_stop').addClass("hidden")
            $('#modal_backing_play').removeClass("hidden")
            maestoso.mixer.stopTracks()
        })

        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });

    }

}