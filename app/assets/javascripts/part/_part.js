maestoso.part = {
    show: function () {
        navigator.getUserMedia({audio: true}, maestoso.RecordSession.startUserMedia, function (e) {
        });

        $('#startPlayback').on('click', function () {
            abcplay.set_g_vol(1)
            abcEditor.play_tune()
            $('#stopPlayback').parent().removeClass('hidden')
            $('.start').parent().addClass('hidden')
        })

        $('#stopPlayback').on('click', function () {
            abcplay.stop();
            $('#stopPlayback').parent().addClass('hidden')
            $('.start').parent().removeClass('hidden')
        })

        $('#startRhythm').on('click', function () {
            if(abcEditor.hittimes.length > 0){
                maestoso.currentTune.notesTohit = abcEditor.hittimes
            }
            maestoso.beatPress.registerEvent()
            maestoso.rhytmPractice.mode = "practice"
            maestoso.rhytmPractice.playAlong = $('#play_along').is(':checked')
            maestoso.rhytmPractice.withMetronome = true
            maestoso.rhytmPractice.bpm = $('#bpm').val()
            maestoso.rhytmPractice.start(maestoso.tempo/maestoso.original_tempo, function(){
                $('#stopRhythm').parent().addClass('hidden')
                $('.start').parent().removeClass('hidden')
            });
            $('#stopRhythm').parent().removeClass('hidden')
            $('.start').parent().addClass('hidden')
        })

        $('#stopRhythm').on('click', function () {
            maestoso.rhytmPractice.stop("failed");
            $('#stopRhythm').parent().addClass('hidden')
            $('.start').parent().removeClass('hidden')
        })

        $('#startRecord').on('click', function () {            
            maestoso.RecordSession.startRecording(this);
            $('#stopRecord').parent().removeClass('hidden')
            $('.start').parent().addClass('hidden')
        })

        $('#stopRecord').on('click', function () {
            maestoso.RecordSession.stopRecording(this);
            $("#modal_recordings").modal()
            if(maestoso.metronome !=undefined){
                maestoso.metronome.stop()
            }
            $('#stopRecord').parent().addClass('hidden')
            $('.start').parent().removeClass('hidden')
        })

        $('.recordings_tab').on('click', function () {            
            console.log("TODO retrieve recordings")
            $.get('/part/' + maestoso.part_id + '/recordings')
        })


        $('#startIntonation').on('click', function () { 
            console.log("init intonation...")
            if(abcEditor.hittimes.length > 0){
                maestoso.currentTune.notesTohit = abcEditor.hittimes
            }
            maestoso.lpPractice = new maestoso.livePlayPractice(50)
            maestoso.lpPractice.mode = "microphone"
            toggleLiveInput()
            maestoso.countdown.countdownPlayerStart($('#bpm').val(), function () {
                maestoso.lpPractice.start();
            })

            $('#stopIntonation').parent().removeClass('hidden')
            $('.start').parent().addClass('hidden')
        })

        $('#stopIntonation').on('click', function () {
            maestoso.lpPractice.stop()
            $('#stopIntonation').parent().addClass('hidden')
            $('.start').parent().removeClass('hidden')
        })

        $('#groove').on('click', function () {
            console.log("grooveurl", maestoso.grooveUrl)
            window.open(maestoso.grooveUrl, "", "toolbar=no,scrollbars=yes,resizable=yes,top=200,left=200,width=800,height=600");
        })

        $('#next_abc').on('click', function () {
            maestoso.part.next_abc()
        })

        $('#prev_abc').on('click', function () {
            maestoso.part.prev_abc()
        })


        if ($('.toolbar_group:visible').length == 0){
            maestoso.phone = true
        }else{
            maestoso.phone = false
        }
        previousWidth = $('.navbar').width()
        abcEditor.user.imagesize = 'width="' + $(".canvas_wrapper").width() + '"'
        abcEditor.edit_init({source: "abc", target: "canvasDiv"})
        abcEditor.render()
        loadhihatBuffer()
        window.addEventListener("resize", this.checkOrientation, false);
        window.addEventListener("orientationchange", this.checkOrientation, false);
        this.detectDevice()

        $('#assign_homework').on("click", function () {
            $("#modal_add_homework").modal()
        })

        $('#bpm').on('change', function(){
            maestoso.tempo = parseInt($(this).val())
        })

        $('#uploadRecording').on('click', function () {
            $("#modal_upload").modal()
        })

        $("[name='recordings_filter']").on('click', function(){
            switch(this.getAttribute('value')){
                case 'all_parts':{
                    $('.recording').removeClass('hidden')
                    return
                }
                case 'this_part': {
                    $('.recording.other').addClass('hidden')
                    $('.recording.current').removeClass('hidden')
                    $('.recording.study_track').addClass('hidden')
                    return
                }
                case 'study_track': {
                    $('.recording.other').addClass('hidden')
                    $('.recording.current').addClass('hidden')
                    $('.recording.study_track').removeClass('hidden')
                    return
                }                    
            }
        })

        $("[name='messages_filter']").on('click', function(){
            switch(this.getAttribute('value')){
                case 'all_parts':{
                    $('.message').removeClass('hidden')
                    return
                }
                case 'this_part': {
                    $('.message.other').addClass('hidden')
                    $('.message.current').removeClass('hidden')
                    return
                }

            }
        })

        if($('.score_container').length > 0) {
            maestoso.part.cfactor = $('.score_container').width() / 1140
            origH = $('svg').height()
            $('svg').height(origH * maestoso.part.cfactor)

            y1_org = $('svg image')[0].getAttribute('y').slice(0, -2)
            $('svg image')[0].setAttribute('y', parseInt(y1_org * maestoso.part.cfactor) + 'px')

            y2_org = $('svg image')[1].getAttribute('y').slice(0, -2)
            $('svg image')[1].setAttribute('y', parseInt(y2_org * maestoso.part.cfactor) + 'px')

            y3_org = $('svg image')[2].getAttribute('y').slice(0, -2)
            $('svg image')[2].setAttribute('y', parseInt(y3_org * maestoso.part.cfactor) + 'px')
        }else{
            maestoso.part.cfactor = 1
        }

        $('#speedFactor').on('change', function(){
            switch($('#selPractice').val()){
                case 'Sync new track': {
                    maestoso.recordHittimes.init(parseFloat($("#speedFactor").val()))
                    return
                }
                case 'Play synced track': {
                    maestoso.part.playAnimations.init(parseFloat($("#speedFactor").val()))
                    return
                }
            }
        })

        $('#fullScreen').on('click', function(){
            $('#exitFullscreen').parent().removeClass("hidden")
            $('#fullScreen').parent().addClass("hidden")
            maestoso.part.fullscreen()
        })

        $('#exitFullscreen').on('click', function(){
            $('#exitFullscreen').parent().addClass("hidden")
            $('#fullScreen').parent().removeClass("hidden")
            maestoso.part.exit_fullscreen()
            $(".canvas_wrapper").animate({"margin-top": "0px"})
        })

    },

    updateBpm: function(bpm){
        $("#abc").val($("#abc").val().replace(/Q:[0-9]+/, "Q:" + bpm))
    },

    initIntonationpractice: function () {
        console.log("init intonation...")

        if(abcEditor.hittimes.length > 0){
            maestoso.currentTune.notesTohit = abcEditor.hittimes
        }
        $('#selStop,#selPauze,#selContinue,#selStartEnd, #speedFactor').parent().addClass('hidden')
        try { maestoso.rhytmPractice.stop() } catch (e) { };
        try { maestoso.lpPractice.stop() } catch (e) { };
        try { player.stopPlay() } catch (e) { };
        try { maestoso.RecordSession.stopRecording(this) } catch (e) { };
        $('.selTrack').parent().addClass('hidden')



        $('#selStart').parent().removeClass('hidden')
        maestoso.lpPractice = new maestoso.livePlayPractice(50)
        maestoso.lpPractice.mode = "microphone"
        toggleLiveInput()
    },

    fullscreen: function(){
        $('.hide_fs').addClass('hidden')
        $('body').addClass('no_padding')
    },
    exit_fullscreen: function(){
        $('.hide_fs').removeClass('hidden')
        $('body').removeClass('no_padding')
    },

    open: function(partId, chapterId){
        window.open("/parts/" + partId + "?chapter_id=" + chapterId, "_self" )
    },

    detectDevice: function () {
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            maestoso.deviceType = 'mobile'
        } else {
            maestoso.deviceType = 'desktop'
        }
        this.previousOrientation = window.orientation;
    },

    checkOrientation: function () {
        console.log("width chekcing",previousWidth, $('#canvasDiv').width() )
        if (previousWidth !== $('.navbar').width()) {
            previousOrientation = window.orientation;
            previousWidth = $('.navbar').width()
            window.setTimeout(function () {
                abcEditor.user.imagesize = 'width="' + $(".canvas_wrapper").width() + '"'
                abcEditor.render()
            }, 500)

        }
    },

    next_abc: function() { 
        if(maestoso.abc_index < maestoso.max_abc_index){
            maestoso.abc_index ++
        } else
        {
            maestoso.abc_index = 0
        }
        maestoso.part.load_abc()
    },

    prev_abc: function() { 
        if(maestoso.abc_index > 0){
            maestoso.abc_index --
        } else
        {
            maestoso.abc_index = maestoso.max_abc_index
        }
        maestoso.part.load_abc()
    },

    load_abc: function(){
        maestoso.part_id = $('#other_abc' + maestoso.abc_index).data('partId');
        $('#abc_form').attr('action', '/parts/' + maestoso.part_id +'.json');
        $('#abc').val($('#other_abc' + maestoso.abc_index).val());
        $('#instruction').html($('#instruction_' + maestoso.abc_index).val());
        abcEditor.render();
    }
}

