maestoso.part.autoscroll = {
    currentScroll: 0,
    scrollIfNeeded: function(svgIndex, y) {
        // add top position of svg in case of abc tune when there are multiple svgs
        if(svgIndex > 0){
            y = y + svgIndex * 100
        }

        distanceToCenter = (y - 200) * maestoso.part.cfactor
        diff = distanceToCenter - this.currentScroll
        if (Math.abs(diff) > 200) {
            if(Math.abs(diff) > 600){
                var scrollSpeed = 200
            }else{
                var scrollSpeed = 3000
            }

            this.currentScroll = distanceToCenter;
            return $(".canvas_wrapper").animate({
                "margin-top": "-=" + parseInt(diff) + "px"
            }, scrollSpeed);
        }
    },

    reset: function(){
      console.log('resetting scroll')
        return $(".canvas_wrapper").animate({
            "margin-top": "+=" + parseInt(this.currentScroll) + "px"
        }, 200);
    },

    scrollToBottomAfter: function(secs){
        window.setTimeout(function(){
            $("html, body").animate({ scrollTop: document.body.scrollHeight }, 10000);
        }, secs * 1000)
    }

}