maestoso.part.annotation = {
  init: function(){
      if(maestoso.annotations!=undefined) {
          // TODO find a better way than a timeout to ensure the svg is there
          window.setTimeout(function(){
              for (var i = 0; i < maestoso.annotations.length; i++) {
                  var ann = maestoso.annotations[i]
                  maestoso.part.annotation.drawText(ann.text, ann.xpos, ann.ypos)
              }

              $("#canvasDiv").on("click", function (e) {
                  if (!$("#annotation_message").hasClass("hidden")) {
                      $("#add_annotation_text").removeClass("hidden")
                      $("#add_annotation_text_field").val("")
                      $("#add_annotation_text_field").focus()
                      $("#add_annotation_text").css("top", e.clientY - 20)
                      $("#add_annotation_text").css("left", e.clientX)

                  }
              })

          }, 500)

          if (maestoso.annotations.length > 0) {
              $("#action_delete_annotations").removeClass("hidden")
          }

          $("#action_delete_annotations").on("click", function () {
              if(maestoso.current_student_id > 0){
                  $.ajax({
                      url: "/annotations/delete_all_for_part/" + maestoso.part_id + "?user_id=" + maestoso.current_student_id,
                      type: 'DELETE'
                  });
                  $(".score_annotation").remove()
              }
          })

          $("#action_add_annotation").on("click", function () {
              $("#action_disable_annotation").removeClass("hidden")
              $("#action_add_annotation").addClass("hidden")
          })

          $("#action_disable_annotation").on("click", function () {
              $("#action_disable_annotation").addClass("hidden")
              $("#action_add_annotation").removeClass("hidden")
          })

          $("#canvasDiv, .score_container, .pdf_overlay").on("click", function (e) {
              if (maestoso.current_student_id > 0 && $('#action_add_annotation').hasClass("hidden")) {
                  $("#add_annotation_text").removeClass("hidden")
                  $("#add_annotation_text_field").val("")
                  $("#add_annotation_text_field").focus()
                  $("#add_annotation_text").css("top", e.clientY - 20)
                  $("#add_annotation_text").css("left", e.clientX)

              }
          })

          $("#saveAnnotation").on("click", function (e) {
              y = $("#add_annotation_text").position().top - $("#canvasDiv, .score_container").offset().top + $(document).scrollTop()
              x = $("#add_annotation_text").position().left - $("#canvasDiv, .score_container").offset().left + 15
              maestoso.part.annotation.drawText($("#add_annotation_text_field").val(), x, y)
              $("#add_annotation_text").addClass("hidden")
              $("#annotation_message").addClass("hidden")
              $.post("/annotations", {
                  user_id: maestoso.current_student_id,
                  part_id: maestoso.part_id,
                  text: $("#add_annotation_text_field").val(),
                  xpos: x,
                  ypos: y
              })
              $("#action_delete_annotations").removeClass("hidden")
          })

          $("#cancelAnnotation").on("click", function (e) {
              $("#add_annotation_text").addClass("hidden")
          })

      }
  },

  drawText: function(text, xpos, ypos){
      $("#canvasDiv, .score_container, .annotation_container").append("<div class='score_annotation' style='position: absolute;top:"+ypos+"px;left:"+xpos+"px;'>"+text+"</div>")
  }
}









