maestoso.part.editor = {
    init: function(){
        if(window.location.search.indexOf("state=edit")!=-1){
            $(".show_edit").removeClass("hidden")
            $(".hide_edit").addClass("hidden")
        }

        $(".editAddGroup").on("click", function(){
            selected =  $("#abc").selection()
            $("#abc").selection('replace', { text: $(this).data().opengroup + selected + $(this).data().closegroup})
        })

        $(".editAddDecoration").on("click", function(){
            selected =  $("#abc").selection()
            $("#abc").selection('insert', { text: "!" + $(this).data().decorate +"!", mode: 'before' })
        })

        if(maestoso.part_id != undefined) {
            var adjustBPM = function () {
                $("#abc").val($("#abc").val().replace(/Q:1\/4=.*/, "Q:1/4=" + 60))
            }

            if(maestoso.part_type == 'abc') {
                adjustBPM();
                // update hittimes for practicing at different speed
                abcEditor.edit_init({source: "abc", target: "canvasDiv"})
                abcEditor.render()

                maestoso.currentTune.notesTohit = abcEditor.hittimes

                matcharr = $("#abc").val().match(/Q:\D*([0-9]{2,3})|Q:\D*\d\/\d=([0-9]{2,3})/)

                if (matcharr != null) {

                    var tempo = parseInt($.grep(matcharr, function (n) {
                        return (n)
                    })[1])
                    $("#bpm").val(tempo);
                }
            }

            $('#metronome').change(function () {
                player.metronome = $('#metronome').prop("checked")
            });

            $("#action_edit").on("click", function () {
                $(window).unbind("keypress");
                $(".show_edit").removeClass("hidden")
                $(".hide_edit").addClass("hidden")
                if(maestoso.part_type == "drum"){
                    $("#groovescribe").height("1000px")
                    window.frames['groovescribe'].contentWindow.root.swapViewEditMode()
                }else {
                    $("#canvasDiv").addClass("canvasDiv_edit")
                    if (maestoso.part_type == "abc") {
                        $("#scanned_upload").addClass("hidden")
                        $("#abc_editor").removeClass("hidden")
                    } else {
                        $("#scanned_upload").removeClass("hidden")
                        $("#abc_editor").addClass("hidden")
                    }
                }
            })

            $("#action_save").on("click", function () {
                $(".show_edit").addClass("hidden")
                $(".hide_edit").removeClass("hidden")
                $("#canvasDiv").removeClass("canvasDiv_edit")
                type = $("[name='tune_type']:checked").val()
                if ($("[name='part[part_type]']").val() == 'abc') {
                    $('#abc_form').submit();
                    window.location.reload();
                } else {
                    if($("[name='part[part_type]']").val() == 'drum'){
                        window.frames['groovescribe'].contentWindow.root.swapViewEditMode()
                        $.ajax( { url: "/parts/" + maestoso.part_id, method: "put",
                            data: { drumtabs: window.frames['groovescribe'].contentWindow.root.grooveUrl  } } );
                    }else {
                        $('#upload_form').submit();
                    }
                }
            })

            $("[name='tune_type']").on("change", function () {
                if ($("[name='tune_type']:checked").val() == 'abc') {
                    $("#abc_editor").removeClass("hidden")
                    $("#scanned_upload").addClass("hidden")
                } else {
                    $("#abc_editor").addClass("hidden")
                    $("#scanned_upload").removeClass("hidden")
                }

            })

            $("#action_cancel").on("click", function () {
                $(".show_edit").addClass("hidden")
                $(".hide_edit").removeClass("hidden")
                $("#scanned_upload").addClass("hidden")
                $("#abc_editor").addClass("hidden")
            })

            //refresh svg on abc change
            $("#abc").on("keyup", function(){
                abcEditor.render()
            })

        }

        $(".tune_tags").select2({
            tags: true,
            placeholder: "Select tag",
            allowClear: true,
            minimumInputLength: 0,
            maximumSelectionSize: 1,
            width: '100%'
        })

    }
}
