maestoso.part.readExcercise = {
    running: false,
    init: function(){
        $('.start-read-excercise').on('click', function(){
           $('#play_next_' + this.dataset.exid).removeClass("hidden")
        })
        
        $('.next-score').on('click', function(){
            maestoso.part.readExcercise.playNext(this.dataset.exid)
        })
        $('.again-score').on('click', function(){
            maestoso.part.readExcercise.playAgain(this.dataset.exid)
        })

        $('.refresh-scores').on('click', function(){
            maestoso.part.readExcercise.refresh()
        })

        $('.choose_param').on('change', function(){
            console.log("fsdfsdfsdfs")
            window.location.hash = 'exid' + this.dataset.exid
            window.location.search = '?key=' +  $('#' + this.dataset.exid + ' .choose_key').val() + '&tempo=' + $('#' + this.dataset.exid + ' .choose_tempo').val()
        })

        maestoso.part.readExcercise.hittimes = {}
        loadhihatBuffer()

    },
    rotate: function (exid, partsArr) {
        $('.show-on-play').addClass('hidden')
        $('.hide-on-play').removeClass('hidden')
        this.running = true
        time = $('#interval_' + exid).val()
        $('#' +exid + ' .show-on-play').removeClass('hidden')
        $('#' +exid + ' .hide-on-play').addClass('hidden')
        if(window.rotateTimer!=undefined){
            console.log("clearing")
            clearInterval(rotateTimer);
        }
        this.partsArr = partsArr.split(',')
        this.playNext(exid)

        if(time > 0){
            rotateTimer = setInterval(function(){
                if(!$('.pause_excercise').is(':checked')){
                    maestoso.part.readExcercise.playNext()
                    maestoso.part.readExcercise.displayNr += 1
                }
                if(maestoso.part.readExcercise.partsArr.length == 0){
                    clearInterval(rotateTimer);
                }
            }, time * 1000)
        }
    },
    playNext: function(exid){
        //console.log("partsArr". maestoso.part.readExcercise.partsArr)
        $('#partcontainer' + maestoso.part.readExcercise.lastDisplayed).addClass("hidden")
        this._stopExcercise()
        partId = maestoso.part.readExcercise.partsArr.pop()
        this.startExcercise(exid, partId)
    },
    
    playAgain: function(exid){
        this.startExcercise(exid, maestoso.part.readExcercise.lastDisplayed)
    },

    stop: function(exid){
        this.running = false
        this._stopExcercise()
        $('#' +exid + ' .show-on-play').addClass('hidden')
        $('#' +exid + ' .hide-on-play').removeClass('hidden')
        $('#refresh_' + exid).removeClass('hidden')
    },

    _stopExcercise: function(){
        maestoso.rhytmPractice.stop();
        if(maestoso.lpPractice !=undefined){
            maestoso.lpPractice.stop();
        }
        if(maestoso.part.readExcercise.lastDisplayed != undefined){
            maestoso.abcObjects['part' + maestoso.part.readExcercise.lastDisplayed].stop()
        }
    },

    startExcercise: function(exid, partId){
        if(partId == undefined){
            console.log("stopping")
            this.stop(exid)
        }else{
            $('#partcontainer' + partId).removeClass("hidden")
            maestoso.part.readExcercise.lastDisplayed = partId
        }

        switch($('#choose_excercise_' + exid).val()){
            case 'finger_practice': {
                maestoso.part.readExcercise.fingerPractice('part' + maestoso.part.readExcercise.lastDisplayed)
                return
            }
            case 'rhythm_practice': {
                maestoso.part.readExcercise.rhythmPractice('part' + maestoso.part.readExcercise.lastDisplayed)
                return
            }
        }

    },
    
    rhythmPractice: function(id){
        this._stopExcercise()
        window.maestoso.currentTune = { notesTohit: maestoso.abcObjects.hittimes[id] }
        maestoso.rhytmPractice.container = id
        maestoso.beatPress.registerEvent()
        maestoso.rhytmPractice.start(1);
    },

    fingerPractice: function(id){
        this._stopExcercise()
        bpm = 60
        window.maestoso.currentTune = { notesTohit: maestoso.abcObjects.hittimes[id] }
        maestoso.lpPractice = new maestoso.fingerPractice(50, id)
        maestoso.metronome = new maestoso.Metronome(bpm, maestoso.sounds.hiHat)
        $('circle').remove()
        maestoso.countdown.countdownPlayerStart(bpm, function () {
            maestoso.abcObjects['part' + maestoso.part.readExcercise.lastDisplayed].play(0, 1000)
            maestoso.lpPractice.start();
        })

    },

    refresh: function(){
        console.log("called refresh")
    }

}