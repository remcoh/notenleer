// abc2svg - ABC to SVG translator
// Copyright (C) 2014-2015 Jean-Francois Moine
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
// play.js for abc2svg-1.5.19 (2016-05-01)
function AbcPlay(i_onend) {
    var onend = i_onend, ac, gain, a_e, o_vol = .2;
    var evt_idx, iend, ctime, a_g = [], startTime;

    function o_end(o, g) {
        a_g.push(g);
        o.disconnect()
    }
    function play_next() {
        var t, ct, e, e2, rect;

        function play_note(f, d, n) {
            //var synth = new Tone.Synth({oscillator: {type:'sawtooth3'}}).toMaster()
            console.log("nday", n, d)
            maestoso.sounds.piano(n, d);
        }

        e = a_e[evt_idx++];

        if (!e || e[0] > iend) {
            console.log("the end?")
            if (onend)onend();
            return
        }
        $('.element').attr('style', 'fill-opacity: 0' )
        $('.element' + e[0]).attr('style', 'fill-opacity: 0.5' )

        ct = e[1]-startCorr;
        while (1) {
            play_note(e[2], e[3], e[5]);
            e2 = a_e[evt_idx];
            if (!e2) {
                t = ct + e[3]
                break
            }
            e = e2;
            t = e[1]-startCorr;
            if (t != ct)break;
            evt_idx++
        }
        ctime += t - ct;
        var elapsedTime = (new Date() - startTime)/1000
        setTimeout(play_next, (ctime - elapsedTime ) * 1e3 - 100)
    }

    this.play = function (istart, i_iend) {
        if (!a_e)return;
        startTime = new Date()

        iend = i_iend;
        evt_idx = 0;
        // wind to the start
        while (a_e[evt_idx] && a_e[evt_idx][0] < istart)
            evt_idx++;
        startCorr = a_e[evt_idx][1]
        ctime = 0
        play_next()
    };
    this.stop = function () {
        abcEditor.render()
        iend = 0
    };
    this.set_g_vol = function (v) {
        gain.gain.value = v
    };
    this.set_o_vol = function (v) {
        o_vol = v;
        for (var i = 0; i < a_g.length; i++)a_g[i].gain.value = v
    };
    var p_time, abc_time, play_factor;
    this.clear = function () {
        a_e = null
    };
    this.add = function (s, k, tuneId) {
        const BAR = 0, GRACE = 4, KEY = 5, NOTE = 8, TEMPO = 14, BASE_LEN = 1536, scale = [0, 2, 4, 5, 7, 9, 11];
        var bmap = [], map = [], i, n, dt, d, g, rep_st_i, rep_st_t, rep_en_i, rep_en_t, rep_en_map = [];

        function key_map(s) {
            for (var i = 0; i < 7; i++)bmap[i] = 0;
            switch (s.k_sf) {
                case 7:
                    bmap[6] = 1;
                case 6:
                    bmap[2] = 1;
                case 5:
                    bmap[5] = 1;
                case 4:
                    bmap[1] = 1;
                case 3:
                    bmap[4] = 1;
                case 2:
                    bmap[0] = 1;
                case 1:
                    bmap[3] = 1;
                    break;
                case-7:
                    bmap[3] = -1;
                case-6:
                    bmap[0] = -1;
                case-5:
                    bmap[4] = -1;
                case-4:
                    bmap[1] = -1;
                case-3:
                    bmap[5] = -1;
                case-2:
                    bmap[2] = -1;
                case-1:
                    bmap[6] = -1;
                    break
            }
            bar_map()
        }

        function bar_map() {
            for (var j = 0; j < 10; j++)for (var i = 0; i < 7; i++)map[j * 7 + i] = bmap[i]
        }

        function pit2f(s, i) {
            var p = s.notes[i].apit + 19, a = s.notes[i].acc;
            if (a)map[p] = a == 3 ? 0 : a;
            p = Math.floor(p / 7) * 12 + scale[p % 7] + map[p];

            return 440 * Math.pow(2, (p - 69 - window.maestoso.transposeNumber[maestoso.user.instrument]) / 12)
        }

        function pit2midi(s, i) {
            var p = s.notes[i].apit + 19, a = s.notes[i].acc;
            if (a)map[p] = a == 3 ? 0 : a;
            p = Math.floor(p / 7) * 12 + scale[p % 7] + map[p];
            return p
        }

        function play_dup(s) {
            var i, n, en_t, dt, e;
            dt = p_time - rep_st_t;
            for (i = rep_st_i; i < rep_en_i; i++) {
                e = a_e[i];
                a_e.push([e[0], e[1] + dt, e[2], e[3]])
            }
        }

        function do_tie(s, i, d) {
            var j, n, s2, pit, note = s.notes[i], tie = note.ti1, end_time;
            pit = note.apit;
            end_time = s.time + s.dur;
            for (s2 = s.next; ; s2 = s2.next) {
                if (!s2 || s2.time != end_time)return d;
                if (s2.type == NOTE)break
            }
            n = s2.notes.length;
            for (j = 0; j < n; j++) {
                note = s2.notes[j];
                if (note.apit == pit) {
                    d += s2.dur / play_factor;
                    note.ti2 = true;
                    return note.ti1 ? do_tie(s2, j, d) : d
                }
            }
            return d
        }

        key_map(k);
        if (!a_e) {
            a_e = [];
            abc_time = rep_st_t = 0;
            p_time = 0;
            rep_st_i = rep_en_i = 0;
            play_factor = (BASE_LEN / 4) * maestoso.tempo/60
        } else if (s.time < abc_time) {
            abc_time = rep_st_t = s.time
        }
        while (s) {
            for (g = s.extra; g; g = g.next) {
                if (g.type == TEMPO && g.tempo_value) {
                    d = 0;
                    n = g.tempo_notes.length;
                    for (i = 0; i < n; i++)d += g.tempo_notes[i];
                    play_factor = d * g.tempo_value / 60
                }
            }
            dt = s.time - abc_time;
            if (dt > 0) {
                p_time += dt / play_factor;
                abc_time = s.time
            }
            switch (s.type) {
                case BAR:
                    if (s.st != 0)break;
                    if (s.bar_type[s.bar_type.length - 1] == ":") {
                        rep_st_i = a_e.length;
                        rep_st_t = p_time;
                        rep_en_i = 0;
                        rep_en_t = 0
                    } else if (s.text && s.text[0] == "1") {
                        rep_en_i = a_e.length;
                        rep_en_t = p_time;
                        bar_map();
                        for (i = 0; i < 7; i++)rep_en_map[i] = bmap[i];
                        break
                    } else if (s.bar_type[0] == ":") {
                        if (rep_en_i == 0) {
                            rep_en_i = a_e.length;
                            rep_en_t = p_time
                        } else {
                            for (i = 0; i < 7; i++)bmap[i] = rep_en_map[i]
                        }
                        play_dup(s);
                        p_time += rep_en_t - rep_st_t
                    }
                    bar_map();
                    break;
                case KEY:
                    if (s.st != 0)break;
                    key_map(s);
                    break;
                case NOTE:
                    d = s.dur / play_factor;
                    for (i = 0; i <= s.nhd; i++) {
                        if (s.notes[i].ti2) {
                            s.notes[i].ti2 = false;
                            continue
                        }
                        if(s.notes[i].ti1){
                            duration = do_tie(s, i, d)
                        }else{
                            duration = d
                        }

                        var freq = pit2f(s, i)
                        var midi = pit2midi(s, i)

                        var MIDI_NUM_NAMES = ["C_1", "C#_1", "D_1", "D#_1", "E_1", "F_1", "F#_1", "G_1", "G#_1", "A_1", "A#_1", "B_1",
                        "C0", "C#0", "D0", "D#0", "E0", "F0", "F#0", "G0", "G#0", "A0", "A#0", "B0",
                        "C1", "C#1", "D1", "D#1", "E1", "F1", "F#1", "G1", "G#1", "A1", "A#1", "B1",
                        "C2", "C#2", "D2", "D#2", "E2", "F2", "F#2", "G2", "G#2", "A2", "A#2", "B2",
                        "C3", "C#3", "D3", "D#3", "E3", "F3", "F#3", "G3", "G#3", "A3", "A#3", "B3",
                        "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4",
                        "C5", "C#5", "D5", "D#5", "E5", "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5",
                        "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6", "G#6", "A6", "A#6", "B6",
                        "C7", "C#7", "D7", "D#7", "E7", "F7", "F#7", "G7", "G#7", "A7", "A#7", "B7",
                        "C8", "C#8", "D8", "D#8", "E8", "F8", "F#8", "G8", "G#8", "A8", "A#8", "B8",
                        "C9", "C#9", "D9", "D#9", "E9", "F9", "F#9", "G9"]

                        var noteName = MIDI_NUM_NAMES[midi]

                        a_e.push([s.istart, p_time, freq, duration, midi, noteName])
                        abcEditor.hittimes.push({index: s.istart, duration: duration*1000, startTime: p_time*1000, endTime: (p_time + duration)*1000, freq: freq, pitch: pit2midi(s, i) })
                        if(maestoso.abcObjects!=undefined){
                            maestoso.abcObjects.hittimes[tuneId].push({index: s.istart, duration: duration*1000, startTime: p_time*1000, endTime: (p_time + duration)*1000, freq: freq, pitch: pit2midi(s, i) })
                        }

                    }
                    break
            }
            s = s.ts_next
        }
    };

    //ac = maestoso.audioContext

    if(window.audioContext == undefined){
        if (window.AudioContext)ac = new window.AudioContext; else if (window.webkitAudioContext)ac = new window.webkitAudioContext; else return {};
    }else
    {
        ac = window.audioContext
    }



    gain = maestoso.audioContext.createGain();
    gain.gain.value = .7;
    if (1) {
        gain.connect(maestoso.audioContext.destination)
    } else {
        comp = maestoso.audioContext.createDynamicsCompressor();
        comp.ratio = 16;
        comp.attack = 5e-4;
        comp.connect(ac.destination);
        gain.connect(comp)
    }
}