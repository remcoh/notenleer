var abcEditor = {
    abc_fname: ["noname.abc", ""],	// file names
    ref:  [],			// source reference array
    colcl:  [],			// colorized classes
    positions: {},
    hittimes: [],
    prefixAbc: "%%leftmargin 10px\n%%rightmargin 10px\n",

    ignore_types: {
        beam: true,
        slur: true,
        tuplet: true
    },

    user: {
        my_img_out: function (str) {
            abcEditor.abc_images += str
        },
        // -- optional methods
        // annotations
        anno_start: function (type, start, stop, x, y, w, h) {
            if (abcEditor.ignore_types[type])
                return
            stop -= start;
            // keep the source reference
            ref.push([start, stop]);
            // create a container for the music element
            abc.out_svg('<g class="e_' + start + '_' + stop + '_">\n')
        },
        anno_stop: function (type, start, stop, x, y, w, h) {
            if (abcEditor.ignore_types[type])
                return
            // close the container
            abc.out_svg('</g>\n');
            // create a rectangle
            abc.out_svg('<rect class="abcr _' + start + '_' + (stop - start) +
                '_ element element' + start +'" x="');
            abc.out_sxsy(x, '" y="', y);
            abc.out_svg('" width="' + w.toFixed(2) +
                '" height="' + h.toFixed(2) + '" data-start="'+start+'"/>\n')
            //store position info for later use
            abcEditor.positions[start] = {x: abc.sx(x), y: abc.sy(y), w: w, h: h}

        },
        // for playing
        get_abcmodel: function (tsfirst, voice_tb, music_types, info) {
            abcplay.add(tsfirst, voice_tb[0].key)
        },

        // -- optional attributes
        page_format: true	// define the non-page-breakable blocks
    },

    edit_init: function (options) {
        // detect mobile device
        if(maestoso.phone) {
            abcEditor.prefixAbc = "%%leftmargin 10px\n%%rightmargin 10px\n%%pagescale 2%%\n"
        }
        abcEditor.hittimes = []
        abcEditor.source = options.source
        abcEditor.target = options.target
        abcEditor.targetWidth =  options.width
        if (typeof abc2svg != "object"
            || !abc2svg.version
            || typeof AbcPlay != "function") {
            setTimeout(abcEditor.edit_init, 500)
            return
        }
        abcplay = new AbcPlay(abcEditor.endplay)
        if (!abcplay.play) {
            delete abcplay;
        }
    },

    render: function() {
        var target = document.getElementById(abcEditor.target)
        abcEditor.user.img_out = abcEditor.user.my_img_out;
        if(abcEditor.targetWidth != undefined){
            abcEditor.user.imagesize =  'width="' + abcEditor.targetWidth + '"'
        }
        abc = new Abc(abcEditor.user);
        abcEditor.abc_images = '';
        abc.tosvg('edit', '%%bgcolor white\n\
%%beginsvg\n\
<style type="text/css">\n\
	rect.abcr {fill:#a08000; fill-opacity:0; z-index: 15}\n\
	rect.abcr:hover {fill-opacity:0.3}\n\
</style>\n\
%%endsvg\n');

        ref = []
        try {
            abc.tosvg(abcEditor.abc_fname[0], abcEditor.prefixAbc + document.getElementById(abcEditor.source).value);
        } catch (e) {
            console.log(e.message + '\nabc2svg tosvg bug - stack:\n' + e.stack)
            return
        }
        try {
            target.innerHTML = abcEditor.abc_images
        } catch (e) {
            console.log(e.message + '\nabc2svg image bug - abort')
            return
        }

        // set callbacks on all abc rectangles
        setTimeout(function () {
            var elts = document.getElementsByClassName('abcr'),
                i = elts.length,
                elt
            while (--i >= 0) {
                elt = elts[i];
                elt.onclick = function () {
                    abcEditor.clickedStartPos = parseInt(this.getAttribute("data-start"))
                    abcEditor.selabc(this)
                }
                elt.onmouseover = function () {
                    abcEditor.m_over(this)
                }
                elt.onmouseout = function () {
                    abcEditor.m_out(this)
                }
            }
        }, 300)
        // update postions for hittimes after rendering
        svgIndex = 0
        lastxPos = 0

        for(i=0; i<abcEditor.hittimes.length; i++) {
            abcEditor.hittimes[i]["xpos"] = abcEditor.positions[abcEditor.hittimes[i].index].x;
            abcEditor.hittimes[i]["ypos"] = abcEditor.positions[abcEditor.hittimes[i].index].y;
            if(abcEditor.hittimes[i]["xpos"] < lastxPos && lastxPos > 0){
                svgIndex = svgIndex + 1
            }
            abcEditor.hittimes[i]["svgIndex"] = svgIndex
            lastxPos = abcEditor.hittimes[i]["xpos"]
        }
    },

    play_tune: function (start) {
        var start
        if (abcEditor.playing) {
            abcplay.stop();
            abcEditor.endplay()
            return
        }
        delete abcEditor.user.img_out
        var abc = new Abc(abcEditor.user);
        abcEditor.playing = true;			// get the schema and stop SVG generation
        abcplay.clear()			// clear all playing events
        try {
            abc.tosvg(abcEditor.abc_fname[0], abcEditor.prefixAbc + document.getElementById(abcEditor.source).value);
        } catch (e) {
            abcEditor.playing = false
            return
        }

        if(abcEditor.clickedStartPos > 0){
            start = abcEditor.clickedStartPos
        } else{
            start = 0
        }
        abcplay.play(start, 100000)		// play all events
    },

    endplay: function () {
        //document.getElementById("playbutton").setAttribute("value", "Play");
        $('#stopPlayback').parent().addClass('hidden');
        $('.start').parent().removeClass('hidden');
        abcEditor.playing = false
    },

    src_change: function (evt) {
        var timer
        clearTimeout(timer);
        timer = setTimeout(abcEditor.render, 1000)
    },

    seltxt: function(elt) {
        var i, n, o, start, end
        if (abcEditor.colcl.length != 0) {
            abcEditor.colorsel("black");
            abcEditor.colcl = []
        }
        if (elt.selectionStart == undefined)
            return
        start = elt.selectionStart;
        end = elt.selectionEnd
        if (start == 0
            && end == document.getElementById(abcEditor.source).value.length)
            return				// select all
        n = ref.length
        for (i = 0; i < n; i++) {
            o = ref[i][0]
            if (o >= start && o < end)
                abcEditor.colcl.push('e_' + o + '_' + ref[i][1] + '_')
        }
        if (abcEditor.colcl.length != 0) {
            abcEditor.colorsel("#ff0000")
            var s = document.getElementById("dright")
            var z = window.document.defaultView.getComputedStyle(s).getPropertyValue('z-index')
            if (z != 10) {			// if select from textarea
                var elts = document.getElementsByClassName(abcEditor.colcl[0]);
                elts[0].scrollIntoView()	// move the element on the screen
            }
        }
    },

    colorsel: function(color) {
        var i, n = abcEditor.colcl.length
        for (i = 0; i < n; i++)
            abcEditor.setcolor(this.colcl[i], color)
    },

    selabc: function(me) {
        // correction because margin %% directives are in abc but not in textfield as expected by the script
        var offset = 37
        var c = me.getAttribute('class'),
            d_s_l_d = c.split('_'),
            i1 = Number(d_s_l_d[1]) - offset,
            i2 = i1 + Number(d_s_l_d[2]),
            s = document.getElementById(abcEditor.source);
        s.focus();
        s.setSelectionRange(i1, i2);
    },

    m_over: function(me) {
        var cl = me.getAttribute('class');
        abcEditor.setcolor(cl.replace('abcr ', 'e'), "#ff0000")
    },

    m_out: function(me) {
        var cl = me.getAttribute('class');
        abcEditor.setcolor(cl.replace('abcr ', 'e'), "black")
    },

    setcolor: function(cl, color) {
        var elts = document.getElementsByClassName(cl),
            i = elts.length,
            elt
        while (--i >= 0) {
            elt = elts[i];
            elt.setAttribute("color", color)
        }
    }
}
