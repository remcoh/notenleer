window.maestoso = {user: {}}
window.maestoso.transposeNumber = {trumpet: 2, flute: 0, piano: 0, flugelhorn: 2, recorder: -12}
//window.maestoso.audioContext = new AudioContext()

maestoso.answer = {
    init: function () {
        maestoso.abcObjects = {}
        maestoso.tempo = 60
        that = this
        $('.btn-check').on("click", function () {
            if (this.dataset.questiontype == "Open" || this.dataset.questiontype == "OpenScore") {
                var val = $('#question_answer_' + this.dataset.questionId).val()
            } else {
                var val = $('input[name="question_answer_' + this.dataset.questionId + '"]:checked').val()
            }
            that.checkAnswer(this.dataset.questionId, val)
        })
        $('.btn-play-rnd').on('click', function () {
            that.playRandomTune(this.dataset.questionId)
        })

        $('.choose').on('click', function () {
            that.checkAbc(this.dataset.questionId, this.dataset.nr)
        }),

        // $('.choose-multiple-choice').on('click', function () {
        //     console.log("chioose",this.getAttribute('value') )
        //     that.checkAnswer(this.dataset.questionId, this.getAttribute('value'))
        // }),

        $('.btn-play-nr').on('click', function () {
            that.playTune(this.dataset.questionId, this.dataset.nr)
        }),

        $('.question-link').on('click', function (e) {
            $(".question_container").addClass("hidden")

        })

        $('#next_question').on('click', function () {
            maestoso.answer.next_question()
        })

        $('#prev_question').on('click', function () {
            maestoso.answer.prev_question()
        })



    },

    checkAnswer: function (question_id, answer) {
        $.post("/questions/" + question_id + "/check_answer", {answer: answer});
    },

    playTune: function (q_id, nr) {
        tuneObject = 'question_' + q_id + '_abc' + nr
        maestoso.abcObjects[tuneObject].play(0, 1000)
    },

    playRandomTune: function (q_id) {
        good = 'question_' + q_id + '_good'
        if (Math.random() > 0.5) {
            maestoso[good] = 2
        } else {
            maestoso[good] = 1
        }
        tuneObject = 'question_' + q_id + '_abc' + maestoso[good]

        maestoso.abcObjects[tuneObject].play(0, 1000)
    },

    checkAbc: function (question_id, nr) {
        $('.question_result_' + question_id).removeClass('glyphicon-thumbs-down')
        $('.question_result_' + question_id).removeClass('glyphicon-thumbs-up')
        if (nr == maestoso['question_' + question_id + '_good']) {
            this._thumbsUp($('#question_result_' + question_id + '_abc' + nr))
        } else {
            this._thumbsDown($('#question_result_' + question_id + '_abc' + nr))
        }

    },


    processAnswer: function (question, result) {
        if (result) {
            $('#question_result_' + question).removeClass('glyphicon-thumbs-down')
                .addClass('glyphicon-thumbs-up')
                .addClass('answerGood')
                .removeClass('answerBad')
        } else {
            $('#question_result_' + question).addClass('glyphicon-thumbs-down')
                .removeClass('glyphicon-thumbs-up')
                .removeClass('answerGood')
                .addClass('answerBad')
        }

    },

    _thumbsUp: function (res) {
        res.removeClass('glyphicon-thumbs-down')
            .addClass('glyphicon-thumbs-up')
            .addClass('answerGood')
            .removeClass('answerBad')
    },

    _thumbsDown: function (res) {
        res.addClass('glyphicon-thumbs-down')
            .removeClass('glyphicon-thumbs-up')
            .removeClass('answerGood')
            .addClass('answerBad')
    },

    load_question: function () { 
        $('.question_container').parent().addClass('hidden')
        $('.question_container_' + maestoso.question_index).removeClass('hidden')

    },

    next_question: function() { 
        if(maestoso.question_index < maestoso.max_question_index){
            maestoso.question_index ++
        } else
        {
            maestoso.question_index = 0
        }
        maestoso.answer.load_question()
    },

    prev_question: function() { 
        if(maestoso.question_index > 0){
            maestoso.question_index --
        } else
        {
            maestoso.question_index = maestoso.max_question_index
        }
        maestoso.answer.load_question()
    },
}