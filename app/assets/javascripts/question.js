maestoso.question = {
    init: function(){
        that = this
        $("#question_type").on("change", function(){
            that.arrangeForm(this.value)
        })
        
        $('#question_ok').on('click', function () {
            $('.question_type_form:hidden').remove() // dont submit hidden sections
            $('.question_form').submit()
        })

        $(".abc_field").on("keyup", function(){
            abcEditor.render()
        })

        $("#question_abc1").on("focus", function(){
           abcEditor.edit_init({source: "question_abc1", target: "svgabc1"})
        })

        $("#question_abc2").on("focus", function(){
            abcEditor.edit_init({source: "question_abc2", target: "svgabc2"})
        })
        
        this.arrangeForm($("#question_type").val())
    },
    
    arrangeForm: function(type){
        $('.question_type_form').addClass('hidden');
        $('.question_type_form input, .question_type_form textarea').attr("disabled", "disabled")
        $('.question_' + type).removeClass('hidden');
        $('.question_' + type + ' input, .question_' + type + ' textarea').removeAttr('disabled')
        abcEditor.edit_init({source: 'question_abc1', target: "svgabc1", width: "600"})
        abcEditor.render()
        abcEditor.edit_init({source: "question_abc2", target: "svgabc2", width: "600" })
        abcEditor.render()
    },

    init_list: function(){
        this.make_taggable()
    },

    make_taggable: function(){
        $(".tune_tags").select2({
            tags: true,
            placeholder: "Select tag",
            allowClear: true,
            minimumInputLength: 0,
            maximumSelectionSize: 1,
            width: '100%'
        })

    }

}