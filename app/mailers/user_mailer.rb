class UserMailer < ApplicationMailer
  default from: 'remco@onzemuziek.nl'
    
  def share_email(message)
    @message = message
    usernames = @message.text.scan(/@\w*/)
    addresses = usernames.collect{ |u| User.where(username: u.slice(1.. -1)).first.email }
    mail(to: addresses, subject: "#{@message.from_user.name} heeft een bericht geplaatst op 'onzemuziek.nl'" ) unless addresses.empty?

  end

  def notify_comment(comment, current_user)
    @comment = comment
    @commentable = comment.commentable
    mail(to: @comment.users_to_notify(current_user).collect(&:email), subject: "#{@comment.user.name} heeft commentaar gegeven op 'onzemuziek.nl'" )
  end

  def notify_upload(upload)
    @upload = upload
    @tune = @upload.part.tune
    mail(to: upload.user.teacher.email, subject: "Je leerling #{@upload.user.name} heeft een opname gemaakt op onzemuziek.nl'" )
  end

  def notify_homework(teacher, student, tune)
    @teacher = teacher
    @student = student
    @tune = tune
    mail(to: student.email, subject: "Je leraar op onzemuziek.nl: #{@teacher.name} heeft je nieuw huiswerk opgegeven" )
  end
   
end
