class Part < ActiveRecord::Base
  belongs_to :tune
  has_many :favorites
  has_many :homeworks
  has_many :recordings
  has_many :messages
  has_many :annotations
  mount_uploader :scan1, ::ScanUploader
  mount_uploader :scan2, ::ScanUploader
  mount_uploader :scan3, ::ScanUploader
  mount_uploader :scan4, ::ScanUploader

  def self.create_from_abc(tune, abc)

    create({ tune_id: tune.id,
             key:  /K:(.*)/.match(abc)[1],
             notation: abc.gsub!(/.*?(?=X:)/im, ""),
           })
  end

  def add_to_favorites(p)
    case p[:for]
    when ""
      return
    when "individual"
      p[:users].each do |u|
        Favorite.create part_id: self.id, user_id: u unless is_favorite_for_user(u)
      end
    when "group"
        Group.find(p[:group_id]).users.each do |u|
          Favorite.create part_id: self.id, user_id: u.id unless is_favorite_for_user(u)
      end
    end
  end

  def is_favorite_for_user(user)
    return false if user.nil?
    Favorite.where(user_id: user.id, part_id: self.id).count > 0
  end

  def title
    tune.title rescue id
  end

  def author
    User.find(author_id)
  end

  def svg_height
    return 7200 unless scan4.url.nil?
    return 5400 unless scan3.url.nil?
    return 3600 unless scan2.url.nil?
    1800
  end

  def annotations_for(user_id)
    annotations.where(user_id: user_id)
  end

end
