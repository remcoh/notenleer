class Book < ActiveRecord::Base
  has_many :chapters, dependent: :destroy
  has_many :book_subscriptions, dependent: :destroy
  mount_uploader :image, ::ImageUploader

  scope :for_user, -> (user) { joins("LEFT OUTER JOIN book_subscriptions on book_subscriptions.book_id = books.id")
                               .where(['book_subscriptions.user_id = ? or books.author_id = ?', user.id, user.id]).uniq
  }

  def author
    User.find(author_id)
  end

end
