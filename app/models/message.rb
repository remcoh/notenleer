class Message < ActiveRecord::Base
  belongs_to :part
    
  def from_user
    User.find(from_user_id)
  end  
  
  def to_user
    User.find(to_user_id)
  end 
      
end
