class Question < ActiveRecord::Base
  self.inheritance_column = :_type_disabled
  acts_as_taggable

  filterrific(
      default_filter_params: { sorted_by: 'created_at' },
      available_filters: [
          :search,
          :type,
          :sorted_by
      ]
  )

  scope :search, -> (l) { where(["lower(text) like ?", "%#{l.downcase}%"]) }
  scope :type, -> (l) { where(["type = ?", l]) }

  scope :sorted_by, ->(column) { order(column) }

  def self.question_type_options
    [['Open question', 'Open'],['Open score question', 'OpenScore'],['Multiple Choice', 'MultipleChoice'], ['Kies partij bij geluid', 'WhichScore'],['Kies geluid bij partij', 'WhichSound']]
  end

end

