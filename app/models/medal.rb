class Medal < ActiveRecord::Base

  belongs_to :user

  MEDALS = { "bronze" => { successor: "silver"}, "silver" => { successor: "gold" },  "gold" => { successor: "platinum" }, "platinum" => { successor: "bronze" } }

  def self.update(medal_name, user, points)
    medal = earning_or_new(user, medal_name)
    medal.points += points
    medal.status = "earned" if medal.color == "bronze" && medal.points >= 10000 ||
                               medal.color == "silver" && medal.points >= 20000 ||
                               medal.color == "gold" && medal.points >=30000 ||
                               medal.color == "platinum" && medal.points >=5000
    medal.save
  end

  def image
    "medals/#{name}_#{color}.png"
  end

  private

  def self.earning_or_new(user, medal_name)
    medal = Medal.where(user_id: user.id, name: medal_name, status: "earning").first
    return medal unless medal.nil?
    last_earned = Medal.where(user_id: user.id, name: medal_name, status: "earned").order(:created_at).last
    color = last_earned.nil? ? "bronze" : MEDALS[last_earned.color][:successor]
    return Medal.new user_id: user.id, name: "reading_parts", status: "earning", color: color, points: 0
  end

end
