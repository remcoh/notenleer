class Homework < ActiveRecord::Base
  belongs_to :user
  belongs_to :part

  scope :for_user, -> (user) { where(:user_id => user.id) }

end
