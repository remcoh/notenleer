class Chapter < ActiveRecord::Base
  belongs_to :book
  acts_as_tree order: 'chapter_nr'
  extend ActsAsTree::TreeWalker

  before_save :process_tune_brackets, :process_question_brackets, :clean_parent_id

  def last_paragraph_nr
    paragraphs.order(:paragraph_nr).last.try(:paragraph_nr) || 0
  end

  private

  def process_tune_brackets
    content.gsub!(/tune\[[^\[]*\]/) do |b|
      code = b[5..-2]
      insert_tune_link(code, 'tune')
    end
    content.gsub!(/tunelink\[[^\[]*\]/) do |b|
      code = b[9..-2]
      insert_tune_link(code, 'tunelink')
    end
  end

  def process_question_brackets
    content.gsub!(/question\[[^\[]*\]/) do |b|
      code = b[9..-2]
      if code.to_i > 0 && Question.find(code).nil? || code.to_i == 0
        q=Question.create(
            text:  code,
            type: 'Open'
        )
        "question[#{q.id}]"
      else
        "question[#{code}]"
      end
    end
  end

  def insert_tune_link(code, type)
    if code.to_i > 0 && Tune.where(id: code).nil? || code.to_i == 0
      code_arr = code.split(";")
      new_tune = Tune.create(
          title:  code_arr[0],
          meter: "4/4",
          tempo: 60,
          author_id: book.author_id
      )
      instrument = code_arr[1] || 'piano'
      new_part = Part.create(
          tune_id: new_tune.id,
          key:  "C",
          part_type: 'abc',
          notation: "X:001&#13T:#{code_arr[0]}&#13M:4/4&#13K:C&#13L:1/8&#13Q:1/4=60&#13P:#{instrument}&#13|",
          instrument: instrument
      )
      "#{type}[#{new_tune.id}]"
    else
      "#{type}[#{code}]"
    end
  end

  def clean_parent_id
    self.parent_id = nil if self.parent_id == ""
  end



end
