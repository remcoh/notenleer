class Composer
  attr_accessor :timesig, :key, :notes, :steps, :measure_parts

  def initialize(timesig, key, notes, steps, measure_parts)
    @timesig = timesig
    @key = key
    @notes = notes
    @steps = steps
    @measure_parts = measure_parts
  end

  def abc
    mparts = {}
    mparts[0] = []
    mparts[1] = []
    ts = @timesig.split('/').collect(&:to_i)
    counts_per_measure = ts[0]
    ticks_per_count = (1.to_f/ts[1]) * 32
    measure_parts_by_length = order_measure_parts(@measure_parts)
    rhythm_to_play = generate_rhythm_from_parts(counts_per_measure, ticks_per_count, measure_parts_by_length)
    return insert_notes_in_rhythm(rhythm_to_play, @notes, @steps)
  end

  private

  def select_note(notes_arr, note, steps)
    curpos = notes_arr.index(note)
    choose_from = notes_arr[[0, curpos + steps[0]].max..curpos + steps[1]]
    choose_from.sample
  end

  def order_measure_parts(measure_parts)
    measure_parts_by_length = {}
    measure_parts.each do |mp|
      length = mp.split(" ").collect{|n| n.gsub('z', '').gsub('(', '').to_i}.sum
      if measure_parts_by_length[length].nil?
        measure_parts_by_length[length] = ["_ #{mp}"]
      else
        measure_parts_by_length[length].push("_ #{mp}")
      end
    end
    measure_parts_by_length
  end

  def  generate_rhythm_from_parts(counts_per_measure, ticks_per_count, measure_parts_by_length)
    rhythm =  []
    rhythm.push '|'
    rhythm_to_play = []
    ticks_per_measure = counts_per_measure * ticks_per_count
    4.times do
      ticks_allocated = 0
      while ticks_allocated < ticks_per_measure do
        available_lengths = measure_parts_by_length.keys
        choose_from = available_lengths.select{ |i| i <= ticks_per_measure - ticks_allocated }
        if choose_from.length > 0
          new_length = choose_from.sample
          rhythm_to_play = rhythm_to_play + measure_parts_by_length[new_length].sample.split(" ")
          ticks_allocated = ticks_allocated + new_length
        else
          (ticks_per_measure - ticks_allocated)/2.to_i.times { rhythm_to_play.push('z2') }
          ticks_allocated = ticks_allocated + ticks_per_measure - ticks_allocated
        end
      end
      rhythm_to_play.push '|'
    end
    rhythm_to_play
  end

  def insert_notes_in_rhythm(rhythm_to_play, notes, steps)
    abc = []
    note = notes.sample
    rhythm_to_play.each do |r|
      (abc.push r; next) if r == '|' ||  r[0] == 'z'
      (abc.push ' '; next) if r == '_'
      if r == '(3'
        triplet_notes = []
        3.times do
          triplet_notes.push(note)
          note = select_note(notes, note, steps)
        end
        abc.push "#{r}#{triplet_notes[0]}4#{triplet_notes[1]}4#{triplet_notes[2]}4"
        next
      end
      abc.push "#{note}#{r}"
      note = select_note(notes, note, steps)
    end
    return abc
  end

end