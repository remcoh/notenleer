class User < ActiveRecord::Base
  include ::Authorization
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  has_many :favorites
  has_many :homeworks
  has_many :recordings
  has_many :book_subscriptions
  has_many :personal_books, class_name: 'Book', through: :book_subscriptions
  has_many :books, as: :group_books, through: :groups
  has_many :user_logs
  has_many :medals
  has_many :groups, foreign_key: :owner_id
  belongs_to :group
  ratyrate_rater

  filterrific(
    default_filter_params: { sorted_by: 'last_name' },
    available_filters: [
    :search,
    :sorted_by
    ]
  )

  scope :with_role, ->(role) { where(:role => role) }
  #scope :search, ->(search) { where(['lower(last_name) like :search or lower(first_name) like :search or lower(email) like :search', :search =>"%#{search.downcase}%"] ) }
  scope :sorted_by, ->(column) { order(column) }
  scope :search, -> (l) { where(["lower(last_name) like ?", "%#{l.downcase}%"]) }
  scope :group_users, -> (user) { where(group_id: user.group_id) }
  scope :teachers_and_admins, -> () { where("role in ('teacher', 'admin')")}

  def password_required?
    super if confirmed?
  end

  def password_match?
    self.errors[:password] << t('user.password_blank') if password.blank?
    self.errors[:password_confirmation] << t('user.password_confirmation_blank') if password_confirmation.blank?
    self.errors[:password_confirmation] << t('user.password_confirmation_match') if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def name
    if [first_name, last_name].include? nil
      email
    else
      "#{first_name} #{last_name}"
    end
  end

  def username
    "#{first_name}_#{last_name}".gsub(" ", "_").downcase
  end

  def students
    return nil unless ['teacher', 'admin'].include? role
    return User.all if role == 'admin'
    User.where(teacher_id: id)
  end

  def books
    return Book.all if role == 'admin'
    if role == 'teacher'
      written_books = Book.where(author_id: id).all
      instrument_books = Book.where(instrument: instrument).all
      written_books + instrument_books
    else
      # TODO student
    end
  end

  def teacher
    return nil if teacher_id.nil?
    User.where(id: teacher_id).first
  end

  def log_event(activity, part, message)
    todays_log = user_logs.where(activity: activity).where(date: Date.today).first
    todays_log = UserLog.new(activity: activity, user_id: self.id, part_id: part.try(:id), date: Date.today) if todays_log.nil?
    todays_log.events = todays_log.events.to_s + "<br>" + message
    todays_log.save
  end

  def grouped_medals
    medals.where(status: 'earned').group(:color).count(:color)
  end

  def student_recordings(student)
    return [] unless ['admin', 'teacher'].include?(role)
    return [] unless student.teacher_id == id
    student.recordings.where("access IN ('group', 'teacher')")#.order('created_at desc')
  end

end
