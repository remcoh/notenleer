class Groove < ActiveRecord::Base
  
    filterrific(
        default_filter_params: { search: '' },
        available_filters: [ :search ]
    )
  
    scope :search, -> (l) { where(["lower(title) like ?", "%#{l.downcase}%"]) }
  
    scope :sorted_by, ->(column) { order(column) }
end
