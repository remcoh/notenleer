class Recording < ActiveRecord::Base
  belongs_to :tune
  belongs_to :part
  belongs_to :user
  has_many :comments, as: :commentable

  mount_uploader :soundtrack, ::SoundtrackUploader
  ratyrate_rateable "performance"


  scope :for_user, -> (user) { where(:user_id => user.id) }

  def identifier
    "#{user.name} #{created_at.strftime("%d-%m-%Y")}"
  end

  def filename
    self.send(:attribute, "soundtrack") || self.id.to_s + ".mp3"
  end

  def soundtrack_base_file_name
    File.basename self.soundtrack.file.filename, ".*"
  end

  def speed_factors
    return [] if speed_versions.nil?
    speed_versions.split(',').each(&:strip!) - ['100']
  end

  def commentable_visit_url
    Rails.application.routes.url_helpers.part_url(self.part) + '#recordings'
  end

  def commentable_title
    "Commentaar"
  end

end
