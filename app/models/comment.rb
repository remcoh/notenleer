class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true
  belongs_to :user

  def users_to_notify(user)
    users = commentable.user == user ? Set[] : Set[commentable.user]
    self.commentable.comments.each do |comment|
      users.add(comment.user) unless comment.user == user
    end
    return users
  end
end
