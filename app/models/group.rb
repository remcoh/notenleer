class Group < ActiveRecord::Base
  has_many :users
  has_many :book_subscriptions
  has_many :books, through: :book_subscriptions

  def self.for_user(user)
    return Group.order('name asc').all if user.role == 'admin'
    user.groups
  end

  def owner
    User.find(owner_id)
  end

  def recordings(current_user)
    Recording.joins('INNER JOIN users on users.id = recordings.user_id')
             .where(['users.group_id = ?', id])
             .where(["recordings.access = 'group' or users.id = ?", current_user.id])
             .order('created_at desc')
  end

end
