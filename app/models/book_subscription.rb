class BookSubscription < ActiveRecord::Base
  belongs_to :user
  belongs_to :book
  belongs_to :personal_book, class_name: 'Book'
end
