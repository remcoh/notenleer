class Tune < ActiveRecord::Base
  has_many :favorites
  has_many :homeworks
  has_many :recordings
  has_many :parts#, dependent: :destroy
  has_many :messages, through: :parts
  has_many :youtube_links
  acts_as_taggable

  filterrific(
  default_filter_params: { sorted_by: 'created_at' },
  available_filters: [
    :sorted_by,
    :with_meter,
    :genre,
    :title,
    :collection
  ]
)

  scope :title, -> (title) { where(["lower(title) like ?", "%#{title.downcase}%"]) }

  scope :sorted_by, -> (column) { order(column) }
  scope :parts, -> { joins(:parts) }
  scope :with_meter, -> (meter) { where(:meter => meter) }
  scope :for_anonymous, -> { limit(20) }
  scope :genre, -> (genre) { where(:genre => genre)}
  scope :collection, -> (collection) { where(:collection => collection)}
  scope :for_user, -> (user) { where("group_id is null or group_id=#{user.group_id}") }
  scope :authored_by, -> (user) { where(author_id: user.id)}
  
  self.per_page = 15
  
  def self.create_from_abc(abc)
    tune = create({ title:  /T:(.*)/.match(abc)[1],
             meter: /M:(.*)/.match(abc)[1],
             tempo:  (/Q:(.*)/.match(abc)[1] rescue 60),
             collection: "abcnotation.com"
             })
    Part.create_from_abc(tune, abc)
    return tune
  end

  def part_for_user(user)
    parts.where(['instrument = ? and part_number = ?', user.instrument, user.part_number]).first || parts[0]
  end

  def number_of_headers
    notation.scan(/[XTCNMLQKPZBROAF]:/).count
  end

  def notation
    super.gsub("\r\n", "\\n").gsub("\n", "\\n")
  end

  def self.genres
    Tune.all.select(:genre).group(:genre).collect(&:genre)
  end

  def self.collections
    Tune.all.select(:collection).group(:collection).collect(&:collection)
  end

  def self.meters
    [ "2/4", "4/4", "3/4", "6/8", "2/2", "3/8" ]
  end

  def self.keys
    %w(C# F# B E A D G C F Bb Eb Ab Db Gb Cb)
  end

  def self.genres
    ActiveRecord::Base.connection.execute("select genre from tunes group by genre").to_a.collect{|g| g['genre']}
  end

  def self.collections
    ActiveRecord::Base.connection.execute("select collection from tunes group by collection").to_a.collect{|g| g['collection']}
  end

  def study_track
    Recording.joins("INNER JOIN parts on parts.id = recordings.part_id")
        .joins("INNER JOIN tunes on tunes.id = parts.tune_id")
        .where(["tune_id = ? and recordings.study_track = true", self.id]).first
  end

  def parts_abc
    parts.collect{|p| p.notation}
  end
end
