module Authorization
  def may_update(object)
    case object.class.name
      when 'Book'
        return true if role == 'admin'
        object.author_id == id
      when 'Tune'
        return true if role == 'admin'
        object.author_id == id
      when 'Part'
        return true if role == 'admin'
        object.author_id == id
      when 'Group'
        return true if role == 'admin'
        object.owner_id == id
      when 'Groove'
        return true if role == 'admin'
        object.owner_id == id        
      when 'User'
        return true if role == 'admin'
        return true if object.teacher_id == id && object.role != 'admin' #is a student
    end
  end

  def may_create(klass)
    case klass.name
      when 'Book'
        return true if ['admin', 'teacher'].include? role
        false
      when 'Group'
        return true if ['admin', 'teacher'].include? role
        false
      when 'Part'
        return true
      when 'Tune'
        return true
      when 'Groove'
        return true        
      when 'YoutubeLink'
        return true if ['admin', 'teacher'].include? role
        false
    end
  end

  def may_destroy(object)
    case object.class.name
      when 'Book'
        return true if role == 'admin'
        object.author_id == id
      when 'Tune'
        return true if role == 'admin'
        object.author_id == id
      when 'Part'
        return true if role == 'admin'
        object.author_id == id
      when 'Question'
        return true if role == 'admin'
        object.author_id == id
      when 'Message'
        return true if role == 'admin'
        object.from_user_id == id
      when 'Groove'
        return true if role == 'admin'
      when 'Group'
        return true if role == 'admin'
        object.owner_id == id
      when 'YoutubeLink'
        return true if ['admin', 'teacher'].include? role
        false
      end
  end

  def may_take_over(user)
    return true if role == 'admin' && user.role != 'admin'
  end
end
